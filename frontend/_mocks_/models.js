
import faker from 'faker';

const PRODUCT_NAME = [
    'Kmeans',
    'Word2vec',
    'MeanSchift',
    'Spectral',
];


const products = [...Array(4)].map((_, index) => {
    return {
        id: faker.datatype.uuid(),
        key: PRODUCT_NAME[index],
        value: PRODUCT_NAME[index],

    };
});

export default products;