import React from "react"

import Form from "components/projects/form"
import Appbar from "components/layouts/appbarForm"

export default function Index() {
  return (
    <Appbar>
      <Form />
    </Appbar>
  )
}
Index.auth = { role: "admin" }
