import React from "react"
import Router from "next/router"
import Box from "@mui/material/Box"
import Container from "@mui/material/Container"
import FormControlLabel from "@mui/material/FormControlLabel"
import Switch from "@mui/material/Switch"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import IconButton from "@mui/material/IconButton"
import Typography from "@mui/material/Typography"
import Stack from "@mui/material/Stack"
import Divider from "@mui/material/Divider"
import Paper from "@mui/material/Paper"
import TextField from "@mui/material/TextField"
import Grid from "@mui/material/Grid"
import { LoadingButton } from "@mui/lab"

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

import { Formik, Form, Field, ErrorMessage } from "formik"
import * as Yup from "yup"
import { updateTraces } from "api/api.traces"

import { notifMessage } from "components/utils/notif"

function Item({ trace, isError, isFetching }) {
  if (isFetching) return <Spinner />
  if (isError) return <Error />

  const initialValues = {
    filename: trace?.filename ?? "",
    public: trace?.public ?? true,
  }

  const validationSchema = Yup.object({
    filename: Yup.string().required("Is required"),
    public: Yup.boolean(),
  })

  const onSubmit = async (values) => {
    const formData = new FormData()
    formData.append("id", trace._id.$oid)
    formData.append("filename", values.filename)
    formData.append("public", values.public)
    try {
      updateTraces(formData)
      notifMessage("success", `Update trace with success !`)
    } catch (error) {
      notifMessage("error", "Failed add or update trace!")
    }
    Router.back()
  }

  return (
    <Container>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Stack direction="row" spacing={2}  alignItems="center" mt={2}>
          <IconButton edge="end" onClick={() => Router.back()}>
            <ArrowBackIcon />
          </IconButton>
          <Typography variant="h6" color="primary">
            Modify trace
          </Typography>
        </Stack>
      </Box>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {(formik) => (
          <Form autoComplete="off" noValidate>
            <Stack spacing={2} sx={{ pt: 2 }}>
              <Paper variant="outlined">
                <Stack>
                  <Box sx={{ p: 2 }}>
                    <TextField
                      id="filename"
                      name="filename"
                      label="filename"
                      fullWidth
                      required
                      value={formik.values.filename}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.filename && formik.errors.filename)}
                      helperText={formik.touched.filename && formik.errors.filename}
                    />
                  </Box>
                  <Box sx={{ p: 2 }}>
                    <TextField
                      id="description"
                      name="description"
                      label="Description"
                      fullWidth
                      multiline
                      maxRows={4}
                      required
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.description && formik.errors.description)}
                      helperText={formik.touched.description && formik.errors.description}
                    />
                  </Box>
                  <Box sx={{ p: 1, display: "flex", alignItems: "center" }}>
                    <FormControlLabel
                      control={
                        <Switch
                          checked={formik.values.public ?? true}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          name="public"
                        />
                      }
                      label="Private"
                      labelPlacement="start"
                    />
                    <Typography sx={{ paddingLeft: 2 }}>Public</Typography>
                  </Box>
                </Stack>
              </Paper>

              <Grid container spacing={0} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={3}>
                  <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                    {trace ? "update" : "Submit"}
                  </LoadingButton>
                </Grid>
              </Grid>
            </Stack>
          </Form>
        )}
      </Formik>
    </Container >
  )
}

export default Item
