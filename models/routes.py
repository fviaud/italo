from flask import request, jsonify
from math import *

from model import Models

def api_routes(app):
    @app.route('/api/v0/models', methods=['GET'])
    def models():
        page = request.args.get('page',1, type=int) 
        limit=request.args.get('limit',10,type=int)
        if limit > 0 :
            totalPages =ceil(Models.objects().count()/limit)
        else :
            totalPages =0
        models = Models.objects().skip((page-1)*limit).limit(limit)
        #Models = Models.objects.paginate(page=page,per_page=limit)
        return jsonify(
            {
                "page":page,
                "results" :list(models),
                "totalPages": totalPages,
                 "documents":Models.objects().count()
            }
            ),200
    
    @app.route('/api/v0/models/<id>', methods=['GET'])
    def model(id):
        try:
            model= Models.objects(pk=id)
            return jsonify(model),200
        except KeyError:
            return 'Format not bvalid in the request body', 400

    @app.route('/api/v0/models', methods=['POST'])
    def create_model():
        try:
            new_model = Models(
                name=request.form['name'],
                description=request.form['description'],
                author=request.form['author'],
            )
           
            new_model.save()
            return jsonify(new_model.to_json()),200
        # except NotUniqueError:
        #     return 'User Already Exists', 400
        # except ValidationError:
        #     return 'Provide an Email and Password valid in the request body', 400
        except KeyError:
            return 'Formatnot bvalid in the request body', 400
    
    @app.route('/api/v0/models/<id>', methods=['DELETE'])
    def delete_model(id):
        try:
            model= Models.objects(pk=id)
            model.delete()
            return jsonify(model.to_json()),200
        except KeyError:
            return 'Formatnot bvalid in the request body', 400
    
        