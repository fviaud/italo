import React from "react"
import { useQuery } from "react-query"

import Cluster from "components/clusters/items"
import Appbar from "components/layouts/appbar"

import { getProject } from "api/api.projects"
import { getClusters, getTestClusters } from "api/api.traces"
import { getJob } from "api/api.jobs"

export default function Index({ params, query }) {
  const { pid, jid } = params
  const { idmodel, idcluster, page, testPage } = query

  const {
    isFetching: isFetchingProject,
    isError: isErrorProject,
    data: PROJECT,
    error: errorProjects,
  } = useQuery("project", () => getProject(pid), { retry: false })

  const fetchObjects = (page = 1) => getClusters(jid, idmodel, idcluster, page)
  const {
    isLoading: isLoadingCluster,
    data: CLUSTER,
    isError: isErrorCluster,
    error: errorCluster,
  } = useQuery(["cluster", page], () => fetchObjects(page), { keepPreviousData: true, retry: false })

  const {
    isLoading: isLoadingTestCluster,
    data: TESTCLUSTER,
    isError: isErrorTestCluster,
    error: errorTestCluster,
  } = useQuery(["testCluster", testPage], () => getTestClusters(jid, idmodel, idcluster, testPage), { keepPreviousData: true, retry: false })

  const { data: job, isLoading: isLoadingJob, isError: isErrorJob } = useQuery("job", () => getJob(jid), { retry: false })

  if (errorCluster?.response.status === 401) Router.push("/login")

  const isLoading = isLoadingJob || isLoadingCluster || isLoadingTestCluster
  const isError = isErrorJob || isErrorCluster || isErrorTestCluster

  return (
    <Appbar>
      <Cluster
        project={PROJECT?.data[0]}
        CLUSTER={CLUSTER}
        TESTCLUSTER={TESTCLUSTER}
        isLoading={isLoading}
        isError={isError}
        query={query}
        page={page}
        testPage={testPage}
        job={job}
        pid={pid}
        jid={jid}
      />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params, query: context.query } }
}

Index.auth = { role: "admin" }
