import React from "react"
import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"

export default function VideoPresentation() {
    return (
        <Box sx={{ maxWidth: "100%", marginTop:4 }}>
            <Typography>ITALO Overview:</Typography>
            <video controls poster="/video/posterPresentationItalo27_09_2022.png" style={{width:"100%"}}>
                <source src="/video/presentationItalo27_09_2022.mp4" type="video/mp4" />
                Your browser does not support the video element. Kindly update it to latest version.
            </video >
        </Box>
    )
}
