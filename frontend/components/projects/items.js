import React from "react"
import Router, { useRouter } from "next/router"
import Image from "next/image"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import FolderIcon from "@mui/icons-material/Folder"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Paper from "@mui/material/Paper"
import Chip from "@mui/material/Chip"
import Pagination from "@mui/material/Pagination"
import Typography from "@mui/material/Typography"
import CircularProgress from "@mui/material/CircularProgress"
import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

import IconButton from "@mui/material/IconButton"
import DeleteIcon from "@mui/icons-material/Delete"

import { notifMessage } from "components/utils/notif"

import { stringToHslColor } from "helpers"

import { delProjects } from "api/api.projects"

export default function Items({ PROJECTS, isLoading, page = 0, isError, refetch }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />
  return (
    <Stack mt={2}>
      {PROJECTS.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {PROJECTS.data.results.map((project, index) => (
          <ListItemButton dense disableRipple key={index} onClick={() => Router.push(`/projects/${project._id.$oid}/jobs`)}>
            <ListItem
              secondaryAction={
                <Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                  <IconButton
                    edge="end"
                    onClick={async (e) => {
                      e.stopPropagation()
                      if (window.confirm("Do you wand delete this object ?")) {
                        try {
                          await delProjects(project._id.$oid)
                          refetch()
                          notifMessage("success", "Delete job with success !")
                        } catch (error) {
                          notifMessage("error", "Failed to delete job !")
                        }
                      }
                    }}>
                    <DeleteIcon />
                  </IconButton>
                </Box>
              }>
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: stringToHslColor(project.name) }}>
                  <FolderIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={
                  <Box sx={{ display: "flex", flexDirection: "row" }}>
                    <Typography variant="body1" gutterBottom sx={{ mr: 1 }}>
                      {project.name[0].toUpperCase() + project.name.substring(1)}
                    </Typography>
                  </Box>
                }
              />
            </ListItem>
          </ListItemButton>
        ))}
      </List>
      {(PROJECTS.data.totalPages > 1 || PROJECTS.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={PROJECTS.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/projects/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
