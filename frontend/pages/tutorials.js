import React from "react"

import Appbar from "components/layouts/appbar"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"
import Box from "@mui/material/Box"

import Tutorial from "components/tutorials"

function Index() {
  return (
    <Appbar>
    <Box sx={{ display: "flex", justifyContent: "space-between" }} pt={2}>
        <Typography variant="h4" color="primary">
          Tutorials
        </Typography>
      </Box>
      <Divider />
      <Tutorial/>
    </Appbar>
  )
}

export default Index
