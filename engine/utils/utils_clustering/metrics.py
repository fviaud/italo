from collections import Counter
import copy
from agilkia import TraceSet
def get_traces_test_distribution_per_cluster(num_clusters,execution_clusters,test_clusters):
    empty = [(i, 0) for i in range(num_clusters)]
    execution_traces_distribution=distrib(execution_clusters,empty)
    test_traces_distribution=distrib(test_clusters,empty)

    return execution_traces_distribution,test_traces_distribution

def distrib(clusters,empty):
    counts = Counter(clusters)
    traces_distribution = copy.deepcopy(empty)
    if clusters is not None:
        for k, v in counts.items():
            traces_distribution[k] = (int(k), int(v))

    return traces_distribution

def get_events_distribution(global_data,num_clusters,traceset_global):
    events_distribution = {}
    events_distribution['global'] = global_data.mean(axis=0).to_dict()
    for i in range(num_clusters):
        list_traces_cluster_i = traceset_global.get_cluster(i)
        traceset_specific_cluster = TraceSet(traces=list_traces_cluster_i)
        # traceset_specific_cluster.set_event_chars(event_chars)
        specific_cluster_data = traceset_specific_cluster.get_trace_data(method="action_counts",
                                                                         columns=global_data.columns)

        events_distribution['cluster' + str(i)] = specific_cluster_data.mean(axis=0).to_dict()
    return events_distribution

def prune_event_distribution(events_ditribution):
    pass
    #todo

