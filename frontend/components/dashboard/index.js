import * as React from "react"
import { experimentalStyled as styled } from "@mui/material/styles"
import Box from "@mui/material/Box"
import Paper from "@mui/material/Paper"
import Grid from "@mui/material/Grid"
import { Stack, Typography } from "@mui/material"
import FolderIcon from "@mui/icons-material/Folder"
import FileCopyIcon from "@mui/icons-material/FileCopy"
import AccountCircleIcon from "@mui/icons-material/AccountCircle"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import DataObjectIcon from "@mui/icons-material/DataObject"
import ModelTrainingIcon from "@mui/icons-material/ModelTraining"

import Item from "./cards"
import { stringToHslColor } from "helpers"

export default function ResponsiveGrid({ projects, datasets, users, models, stores, isLoading, isError }) {
  if (isLoading) return <Spinner />
  if (isError) return <Error />

  const info = [
    { name: "Projects", icon: <FolderIcon />, data: projects?.documents, path: "/projects", color: stringToHslColor("Projects") },
    // {
    //   name: "Datasets",
    //   icon: <FileCopyIcon />,
    //   data: datasets?.documents || 0,
    //   path: "/datasets",
    //   color: stringToHslColor("Datasets"),
    // },
    // {
    //   name: "Traces",
    //   icon: <DataObjectIcon />,
    //   data: stores?.documents,
    //   path: "/traces",
    //   color: stringToHslColor("Stores"),
    // },
    { name: "Models", icon: <ModelTrainingIcon />, data: models?.documents, path: "/models", color: stringToHslColor("Models") },
    { name: "Users", icon: <AccountCircleIcon />, data: users?.documents, path: "/users", color: stringToHslColor("Users") },
  ]
  return (
    <>
      <Paper sx={{ mt: 2, mb: 5 }} elevation={0}>
        <Typography variant="h5" sx={{ p: 2 }}>
          Welcome back !
        </Typography>
        <Typography sx={{ p: 2 }}>ITALO means Inference of end-to-end Test with Artificial intelligence on LOgs</Typography>
      </Paper>
      <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
        {info.map((item, index) => (
          <Grid item xs={2} sm={4} md={4} key={index}>
            <Item item={item} />
          </Grid>
        ))}
      </Grid>
    </>
  )
}
