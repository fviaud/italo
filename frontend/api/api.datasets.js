import axios from "axios"

const api = axios.create({
    baseURL: `${process.env.NEXT_PUBLIC_API_DATASETS}`,
    withCredentials: true,
})

export const getDatasets = (page = 0, limit = 0, fields = []) => {
    return api.get(`datasets`,{params:{page,limit,fields}})
}

export const getDataset = (id) => {
    return api.get(`datasets/${id}`)
}

export const addDatasets = (object) => {
    return api.post("datasets", object)
}

export const updateDatasets = (object) => {
    return api.patch("datasets", object)
}

export const delDatasets = (id) => {
    return api.delete(`datasets/${id}`)
}