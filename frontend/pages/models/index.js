import React from "react"
import { useQuery } from "react-query"
import Link from "next/link"

import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import Button from "@mui/material/Button"

import Models from "components/models/items"
import Appbar from "components/layouts/appbar"

import { getModels } from "api/api.models"

export default function Index({ query }) {
  const page = query.page
  const fetchObjects = (page = 1) => getModels(page)
  const {
    data: MODELS,
    isLoading,
    isError,
    refetch,
  } = useQuery(["models", page], () => fetchObjects(page), { keepPreviousData: true, refetchInterval: 5000 })
  return (
    <Appbar>
      <Box sx={{ display: "flex", justifyContent: "space-between" }} mt={2}>
        <Typography variant="h4" color="primary">
          Models
        </Typography>
        <Link href={"/models/new/"} style>
          <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
            Add
          </Button>
        </Link>
      </Box>
      <Divider />
      <Models MODELS={MODELS} page={page} isLoading={isLoading} isError={isError} refetch={refetch} />
    </Appbar>
  )
}
export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

Index.auth = { role: "admin" }
