from flask import Flask
from flask_login import LoginManager
from flask_bcrypt import Bcrypt

from common import common_routes

# from providers.credentials import credentials_routes
from providers.google import google_routes
from providers.orange import orange_routes
from providers.facebook import facebook_routes

from providers.model import db,bcrypt

import os

app = Flask(__name__)
app.config.from_object('config')

db.init_app(app)
bcrypt.init_app(app)

common_routes(app)
# credentials_routes(app)
google_routes(app)
orange_routes(app)
facebook_routes(app)