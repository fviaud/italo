import * as React from "react"
import { useRouter } from "next/router"
import Link from "next/link"
import { styled } from "@mui/material/styles"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell, { tableCellClasses } from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import OfflineShareIcon from "@mui/icons-material/OfflineShare"
import Grid from "@mui/material/Grid"

import Box from "@mui/material/Box"

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.black,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    // backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}))

function createData(Model, Diversity, Controllabilty, ExecutionTime) {
  return { Model, Diversity, Controllabilty, ExecutionTime }
}

export default function BasicTable({ job }) {
  const router = useRouter()

  const rows = job.generativeModels.map((model) =>
    createData(
      model.name,
      job.generativeModels.find((model2) => model2.name === model.name).results.usage,
      job.generativeModels.find((model2) => model2.name === model.name).results.diversity,
      job.generativeModels.find((model2) => model2.name === model.name).results.execution_time
    )
  )

  return (
    <>
      <Box mt={1} mb={2}>
        <Typography variant="h5" color={"primary"}>
          Test Generation
        </Typography>
      </Box>
      <Typography>
        Supervised learning models after training and inference, have determined the list of the most likely scenarios of your
        system from your execution traces. These scenarios can be adapted to become regression testing suites. Each cluster
        (corresponding to a specific behavior) contains a list of tests. Click on the right side of each model to see the most
        likely scenarios !
      </Typography>

      <TableContainer>
        <Table sx={{ minWidth: 350 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Model Generation</StyledTableCell>
              <StyledTableCell align="right">Usage</StyledTableCell>
              <StyledTableCell align="right">Diversity</StyledTableCell>
              <StyledTableCell align="right">ExecutionTime</StyledTableCell>
              <StyledTableCell align="right"></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <StyledTableRow key={row.Model} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                <StyledTableCell component="th" scope="row">
                  <Link href={`/projects/generationmodels?idJob=${job._id.$oid}&model=${row.Model}`}>
                    <a>{row.Model}</a>
                  </Link>
                </StyledTableCell>
                <StyledTableCell align="right">{row.Diversity}</StyledTableCell>
                <StyledTableCell align="right">{row.Controllabilty}</StyledTableCell>
                <StyledTableCell align="right">{row.ExecutionTime}</StyledTableCell>
                {/* <StyledTableCell align="right">
                  <IconButton
                    // onClick={() => {
                    //   router.push(`${job._id.$oid}/${row.Model}`)
                    // }}>
                    onClick={() => {
                      router.push(`/projects/generationmodels?idJob=${job._id.$oid}&model=${row.Model}`)
                    }}>
                    <OfflineShareIcon />
                  </IconButton>
                </StyledTableCell> */}
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}
