import React from "react"
import Link from "next/link"
import Router, { useRouter } from "next/router"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import IconButton from "@mui/material/IconButton"
import DeleteIcon from "@mui/icons-material/Delete"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Pagination from "@mui/material/Pagination"
import DataObjectIcon from "@mui/icons-material/DataObject"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import Typography from "@mui/material/Typography"

import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"
import { delStores } from "api/api.stores"
import { stringToHslColor } from "helpers"

export default function Items({ STORES, isLoading, page = 0, refetch, isError, upLoading, pid }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />
  return (
    <Stack mt={2}>
      {STORES.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {STORES.data.results.map((store, index) => (
          <ListItemButton dense disableRipple key={index}>
            <ListItem
              secondaryAction={
                <>
                  <IconButton
                    edge="end"
                    onClick={async (e) => {
                      e.stopPropagation()
                      if (window.confirm("Do you wand delete this object ?")) {
                        try {
                          await delStores(store._id.$oid)
                          refetch()
                          notifMessage("success", "Delete store with success !")
                        } catch (error) {
                          notifMessage("error", "Failed to delete store !")
                          refetch()
                        }
                      }
                    }}>
                    <DeleteIcon />
                  </IconButton>
                </>
              }>
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: stringToHslColor(store.name) }}>
                  <DataObjectIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={<>{store.name}</>} secondary={<>{store.description}</>} />
            </ListItem>
          </ListItemButton>
        ))}
      </List>
      {(STORES.data.totalPages > 1 || STORES.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={STORES.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/projects/${pid}/traces/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
