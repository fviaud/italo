import React from "react"
import { useQuery } from "react-query"
import Link from "next/link"

import Stack from "@mui/material/Stack"
import Avatar from "@mui/material/Avatar"
import FolderIcon from "@mui/icons-material/Folder"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"
import Tabs from "@mui/material/Tabs"
import Tab from "@mui/material/Tab"
import Divider from "@mui/material/Divider"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"

import { getStores } from "api/api.stores"
import { getProject } from "api/api.projects"

import Appbar from "components/layouts/appbar"
import Traces from "components/stores/items"
import { stringToHslColor } from "helpers"

function LinkTab(props) {
  return (
    <Link href={props.href}>
      <Tab component="a" {...props} sx={{ textTransform: "none" }} />
    </Link>
  )
}

function Index({ params, query }) {
  const { pid } = params

  const { isFetching, isError, data: PROJECT, error } = useQuery("project", () => getProject(pid), { retry: false })

  const page = query.page || 1

  const fetchObjects = (page = 1) => getStores(page, 10, pid, ["description", "name", "project"])

  const {
    isLoading: isLoadingStores,
    isError: isErrorStores,
    error: errorStores,
    data: STORES,
    refetch,
  } = useQuery(["stores", page], () => fetchObjects(page), { keepPreviousData: true })

  const [value, setValue] = React.useState(1)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <Appbar>
      <Stack mt={2}>
        <Stack direction="row" spacing={2}>
          <Avatar sx={{ bgcolor: stringToHslColor(PROJECT?.data[0].name) }}>
            <FolderIcon />
          </Avatar>
          <Typography variant="h4" color="primary">
            {/* {PROJECT?.data[0].name} */}
            {PROJECT?.data[0].name[0].toUpperCase() + PROJECT?.data[0].name.substring(1)}
          </Typography>
        </Stack>

        <Stack>
          <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
            <Tabs value={value} onChange={handleChange} aria-label="nav tabs example">
              <LinkTab key={0} label="Jobs" href={`/projects/${pid}/jobs`} />
              <LinkTab key={1} label="Traces" href={`/projects/${pid}/traces`} />
              <LinkTab key={2} label="Members" href={`/projects/${pid}/members`} />
            </Tabs>
            <Link href={`/projects/${pid}/traces/new/`} style>
              <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
                Add
              </Button>
            </Link>
          </Box>
          <Divider />
          <main>
            <Traces
              STORES={STORES}
              isLoading={isLoadingStores}
              refetch={refetch}
              pid={pid}
              isError={isErrorStores}
              error={errorStores}
            />
          </main>
        </Stack>
      </Stack>
    </Appbar>
  )
}

export async function getServerSideProps(context) {
  return { props: { params: context.params, query: context.query } }
}

export default Index

Index.auth = { role: "admin" }
