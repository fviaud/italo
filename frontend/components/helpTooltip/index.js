import React from 'react';
import {
    IconButton
} from '@mui/material';
import Tooltip, { tooltipClasses } from '@mui/material/Tooltip';
import HelpOutlineIcon from '@mui/icons-material//HelpOutline';
import { styled } from '@mui/material/styles';

const PersonalisedTooltip = styled(({ className, ...props }) => (
    <Tooltip {...props} classes={{ popper: className }} />
  ))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
      fontSize: 15,
    },
  }));

const HelpTooltip = ({ label, placement = "right", color = "rgba(0, 0, 0, 0.6)" }) => {
    return (
        <>
            <PersonalisedTooltip
            title={label}
            placement={placement}
            arrow
            >
                <IconButton
                    color="primary"
                    aria-label="help"
                    component="span"
                    sx={{
                            "&:hover":{
                                backgroundColor: "transparent"
                            },
                            padding: "2px",
                            color: color,
                            marginLeft: 1
                        }
                    }
                >
                    <HelpOutlineIcon />
                </IconButton>
            </PersonalisedTooltip>
        </>
    );
}

export default HelpTooltip;