import os

MONGODB_SETTINGS = {
    'db': os.environ.get('DATABASE'),
    'host': os.environ.get('MONGODB_URI'),
    'port': int(os.environ.get('MONGODB_PORT'))
}
