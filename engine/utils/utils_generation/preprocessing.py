from agilkia import TraceSet,Trace
import agilkia
import copy
def load_from_dict(cls,data) -> 'TraceSet':
    """Load traces from the given file.
    This upgrades older trace sets to the current version if possible.
    """
    if isinstance(data, dict) and data.get("__class__", None) == "TraceSet":
        return cls.upgrade_json_data(data)
    else:
        raise Exception("unknown JSON file format: " + str(data)[0:60])


def prepare_dataset(execution_traces_dict,test_traces_dict,execution_clusters,test_clusters):
    tests_not_empty = True
    if len(list(test_traces_dict.keys())) == 0:
        tests_not_empty = False

    print("type",type(execution_clusters))
    # Import of execution traces and test traces into a TraceSet Object
    traceset_execution = load_from_dict(TraceSet, execution_traces_dict)
    if tests_not_empty:
        traceset_tests = load_from_dict(TraceSet, test_traces_dict)
    else:
        traceset_tests = TraceSet(traces=[])

    # Merging the execution traces and test traces into a single TracesetObjet
    traceset_global = copy.deepcopy(traceset_execution)
    traceset_global.extend(traceset_tests.traces)

    global_clusters=execution_clusters+test_clusters

    for i, tr in enumerate(traceset_global):
        session_id = tr.events[0].meta_data['sessionID']
        event = agilkia.Event('<sos_' + str(global_clusters[i]) + '>', {}, {}, {'sessionID': session_id})
        tr.events = [event] + tr.events

    ex = agilkia.TracePrefixExtractor()
    X = ex.fit_transform(traceset_global)
    y = ex.get_labels()

    return traceset_global,X,y,ex