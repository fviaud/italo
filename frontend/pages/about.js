import React from "react"

import Appbar from "components/layouts/appbar"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"
import Box from "@mui/material/Box"

import About from "components/about"

function Index() {
  return (
    <Appbar>
    <Box sx={{ display: "flex", justifyContent: "space-between" }} pt={2}>
        <Typography variant="h4" color="primary">
          About
        </Typography>
      </Box>
      <Divider />
      <About/>
    </Appbar>
  )
}

export default Index