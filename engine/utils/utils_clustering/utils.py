from sklearn.cluster import DBSCAN,SpectralClustering

def top_10_value_dict(x):
    l=[k for k, v in sorted(x.items(), key=lambda item: item[1],reverse=True)]
    return l[:10]
def reformat_event_distrib(events_distribution):
    reformat={}
    for k,v in events_distribution.items():
        reformat[k]={}
    for id_cluster,v in events_distribution.items():
        reformat[id_cluster]['global']={}
        reformat[id_cluster]['current']={}
        for action,value in v.items():
            reformat[id_cluster]['global'][action]=value['global']
            reformat[id_cluster]['current'][action]=value['current']
    return reformat
def prune_event_distribution(events_distribution,num_clusters):
    pruned_dict={}
    for i in range(num_clusters):
        pruned_dict['cluster'+str(i)]={}
        ratio={}
        for action,mean in events_distribution['cluster'+str(i)].items():
            ratio[action]=mean/events_distribution['global'][action]
        top_action=top_10_value_dict(ratio)
        for action in top_action:
            pruned_dict['cluster'+str(i)][action]={}
            pruned_dict['cluster'+str(i)][action]['global']=events_distribution['global'][action]
            pruned_dict['cluster'+str(i)][action]['current']=events_distribution['cluster'+str(i)][action]
            # pruned_dict['cluster'+str(i)][action]=events_distribution['cluster'+str(i)][action]

    # pruned_dict['global']=events_distribution['global']
    return reformat_event_distrib(pruned_dict)





class DBSCANWrapper(DBSCAN):
    def predict(self,X):
      return self.labels_.astype(int)
class SpectralClusteringWrapper(SpectralClustering):
    def predict(self,X):
      return self.labels_.astype(int)
