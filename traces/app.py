from flask import Flask

from model import db
from routes import api_routes

app = Flask(__name__)
app.config.from_object('config')

db.init_app(app)

api_routes(app)