from flask import request, jsonify
from bson import ObjectId

import json
from math import *

from model import Users

def api_routes(app):
    @app.route('/api/v0/users', methods=['GET'])
    def users():
        page = request.args.get('page',1, type=int) 
        limit=request.args.get('limit',10,type=int)
        if limit > 0 :
            totalPages =ceil(Users.objects().count()/limit)
        else :
            totalPages =0
        users = Users.objects().skip((page-1)*limit).limit(limit)
        return jsonify(
            {
                "page":page,
                "results" :list(users),
                "totalPages": totalPages,
                "documents":Users.objects().count(),
            }
            ),200
    
    @app.route('/api/v0/users/<id>', methods=['GET'])
    def user(id):
        try:
            user= Users.objects(pk=id)
            return jsonify(user),200
        except KeyError:
            return 'Format not bvalid in the request body', 400


    @app.route('/api/v0/users/<id>', methods=['DELETE'])
    def delete_user(id):
        try:
            user= Users.objects(pk=id)
            user.delete()
            return jsonify(user.to_json()),200
        except KeyError:
            return 'Format not bvalid in the request body', 400
    
        