import * as React from "react"
import Box from "@mui/material/Box"
import Tab from "@mui/material/Tab"
import TabContext from "@mui/lab/TabContext"
import TabList from "@mui/lab/TabList"
import TabPanel from "@mui/lab/TabPanel"
import Chip from "@mui/material/Chip"
import Typography from "@mui/material/Typography"
import Grid from "@mui/material/Grid"
import Stack from "@mui/material/Stack"

import Chart from "components/charts"

export default function LabTabs({ job, tab }) {
  const [value, setValue] = React.useState(job.models[0].name)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  return (
    <Stack spacing={2}>
      <Typography variant="h5" color={"primary"}>
        Identification of Missing Tests
      </Typography>
      <Typography>
        Clustering allows you to identify the missing tests. Various clustering models (Machine Learning) have split your
        execution traces and test traces into groups of similarity. These groups are called clusters and can be assimilated to
        specific behaviors of your application. In the chart below, you can see the volume of execution traces and test traces
        belonging to each cluster. If a cluster contains execution traces but no test traces, you could need to test this specific
        behavior. Click on the bars to see details. Each clustering model has a score. The higher the score is, the better the
        model has split the traces in a relevant way. But be careful, nothing is better than domain-knowledge !
      </Typography>

      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            {job.models.map((item, index) => (
              <Tab
                style={{ textTransform: "none" }}
                key={index}
                label={
                  <div>
                    {item.name}
                    <Chip size="small" label={Math.round(item.results.score * 100)} />
                  </div>
                }
                value={item.name}
              />
            ))}
          </TabList>
        </Box>
        {job.models.map((item, index) => (
          <TabPanel value={item.name} key={index}>
            <Box sx={{ p: 5 }}>
              <Chart job={job} model={item} indexModel={index} />
            </Box>
          </TabPanel>
        ))}
      </TabContext>
    </Stack>

    // </Box>
  )
}
