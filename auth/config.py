import os

SECRET_KEY = os.environ.get('SECRET')

GOOGLE_CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID')
GOOGLE_CLIENT_SECRET = os.getenv('GOOGLE_CLIENT_SECRET')

FACEBOOK_CLIENT_ID = os.getenv('FACEBOOK_CLIENT_ID')
FACEBOOK_CLIENT_SECRET = os.getenv('FACEBOOK_CLIENT_SECRET')

MONGODB_SETTINGS = {
    'db': os.environ.get('DATABASE'),
    'host': os.environ.get('MONGODB_URI'),
    'port': int(os.environ.get('MONGODB_PORT'))
}
