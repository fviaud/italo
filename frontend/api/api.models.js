import axios from "axios"

const api = axios.create({
    baseURL: `${process.env.NEXT_PUBLIC_API_MODELS}`,
    withCredentials: true,
})

export const getModels = (page = 0, limit = 0) => {
    return api.get(`models?page=${page}&limit=${limit}`)
}

export const getModel = (id) => {
    return api.get(`models/${id}`)
}

export const addModels = (object) => {
    return api.post("models", object)
}

export const updateModels = (object) => {
    return api.patch("models", object)
}

export const delModels = (id) => {
    return api.delete(`models/${id}`)
}