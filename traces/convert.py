import csv
from pathlib import Path
from datetime import datetime, date, time
import agilkia

from agilkia import TraceEncoder,TraceSet
import json

def traceset_to_dict(traceset :TraceSet) -> dict:
    str_dict = json.dumps(traceset, cls=TraceEncoder)
    return json.loads(str_dict)


def script_scanette(input_file) -> agilkia.TraceSet:
    # print("now=", datetime.now().timestamp())

    trace1 = agilkia.Trace([])
    #for line in csv.reader(input_file):
    for line in csv.reader(input_file.read().decode('utf8').splitlines()):
        # we ignore the line id.
        timestr = line[1].strip() 
        timestamp = date.fromtimestamp(int(timestr) / 1000.0)
        # print(timestr, timestamp.isoformat())
        sessionID = line[2].strip()
        objInstance = line[3].strip()
        action = line[4].strip()
        paramstr = line[5].strip()
        result = line[6].strip()
        # now we identify the main action, inputs, outputs, etc.
        if paramstr == "[]":
            inputs = {}
        else:
            if  paramstr.startswith("[") and paramstr.endswith("]"):
                paramstr = paramstr[1:-1]
            inputs = {"param" : paramstr}
        if result == "?":
            outputs = {}
        else:
            outputs = {'Status': float(result)}
        others = {
                'timestamp': timestamp,
                'sessionID': sessionID,
                'object': objInstance
                }
        event = agilkia.Event(action, inputs, outputs, others)
        trace1.append(event)
       
    traceset = agilkia.TraceSet([])
    traceset.append(trace1)
    traceset=traceset.with_traces_grouped_by("sessionID", property=True)
    traceset=traceset_to_dict(traceset) 
    return traceset

def script_teaming(input_file) -> agilkia.TraceSet:
    pass