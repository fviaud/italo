import React from "react"
import { useQuery } from "react-query"
import { useQueryClient } from "react-query"
import Link from "next/link"
import Button from "@mui/material/Button"

import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import Divider from "@mui/material/Divider"

import Models from "components/models/items"
import Acount from "components/account/"
import Appbar from "components/layouts/appbar"

import { getModels } from "api/api.models"
import { getUser } from "api/api.users"

export default function Index() {
  const queryClient = useQueryClient()
  const curentSession = queryClient.getQueryData("session")

  const {
    data: USER,
    isLoading: isLoadingUser,
    isError: isErrorUser,
  } = useQuery("user", () => getUser(curentSession.data._user_id), { keepPreviousData: true })
  const { data: MODELS, isLoading, isError, refetch } = useQuery("models", () => getModels(0, 0), { keepPreviousData: true })

  return (
    <Appbar>
      <Acount user={USER?.data[0]} isLoading={isLoadingUser} isError={isErrorUser} />
      <Box sx={{ display: "flex", justifyContent: "space-between" }} mt={2}>
        <Typography variant="h4" color="primary">
          Models
        </Typography>
        <Link href={"/models/new/"} style>
          <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
            Add
          </Button>
        </Link>
      </Box>
      <Divider />
      <Models MODELS={MODELS} isLoading={isLoading} isError={isError} refetch={refetch} />
    </Appbar>
  )
}
Index.auth = { role: "admin" }
