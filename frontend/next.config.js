module.exports = {
    env: {
        NEXT_PUBLIC_API_AUTH: "/api/auth",
        NEXT_PUBLIC_API_PROJECTS: "/api/v0/",
        NEXT_PUBLIC_API_DATASETS: "/api/v0/",
        NEXT_PUBLIC_API_TRACES: "/api/v0/",
        NEXT_PUBLIC_API_MODELS: "/api/v0/",
        NEXT_PUBLIC_API_USERS: "/api/v0/",
    },
}
