
from datetime import datetime
from math import *
import os
import time
import gridfs 
import json
import copy
import traceback

from enum import Enum

from utils.db import get_db,read_db,update_db_clustering,update_db_generation,update_db_change_status_project
from utils.dataset import get_clustering_dataset
from utils.utils_clustering.clustering import clustering_pipeline
from utils.utils_generation.generation import generation_pipeline
# mode='LOCAL_DEBUG' #possible values LOCAL_DEBUG or SERVER
mode='SERVER' #possible values LOCAL_DEBUG or SERVER

# COLLECTION="projects"
COLLECTION="jobs"

while True :
    db, collection, collection_traces, collection_file,collection_datasets,collection_stores=get_db(mode,collection_name=COLLECTION)
    data=read_db(mode,collection)
    for item in data:
        try:
            best_clustering_score=0
            best_clustering_results={}
            data_json1,data_json2=get_clustering_dataset(mode,item,db,collection_traces,collection_file,collection_datasets,collection_stores)
            for model in item['models']:
                print("Start process : ",model['name'])
                clustering_results=clustering_pipeline(model['name'],copy.deepcopy(data_json1),copy.deepcopy(data_json2))
                if clustering_results['score']>best_clustering_score:
                    best_clustering_score=clustering_results['score']
                    best_clustering_results=clustering_results
                project=update_db_clustering(mode,collection,item,model,clustering_results)

            for generative_model in item['generativeModels']:
                generative_results=generation_pipeline(generative_model['name'],None,None,copy.deepcopy(data_json1),copy.deepcopy(data_json2),best_clustering_results['execution_clusters'],best_clustering_results['test_clusters'],item['nbScenarios'])
                project=update_db_generation(mode,collection,item,generative_model,generative_results)
                
            update_db_change_status_project(mode,collection,item,"completed")


        except Exception:
            traceback.print_exc()

        except :
            update_db_change_status_project(mode,collection,item,"failed")

    if mode=='LOCAL_DEBUG':
        break
    time.sleep(10)

