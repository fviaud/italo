from flask import redirect
from flask_login import login_user
from authlib.integrations.flask_client import OAuth

from .model import Users

import os

NAME="google"
SCOPE='openid email profile'
CONF_URL='https://accounts.google.com/.well-known/openid-configuration'

def google_routes(app):
    oauth = OAuth(app)
    oauth.register(
        name=NAME,
        server_metadata_url=CONF_URL,
        client_kwargs={'scope': SCOPE}
    )

    @app.route('/api/auth/login/google')
    def login_google():
        return oauth.google.authorize_redirect(os.environ.get('GOOGLE_REDIRECT_URI'))

    @app.route('/api/auth/callback/google')
    def auth_google():
        token = oauth.google.authorize_access_token()
        userinfo = token.get('userinfo')
        if userinfo:
            user=Users.objects(email=userinfo['email']).first()
            if not user:
                user = Users(
                    email=userinfo['email'],
                    name=userinfo['name'])
                user.save()
            user.id = user.to_json()["id"]
            login_user(user)
        return redirect('/')