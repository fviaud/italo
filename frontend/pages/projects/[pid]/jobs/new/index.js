import React from "react"
import { useQuery } from "react-query"
import Router from "next/router"

import Form from "components/jobs/form"
import Appbar from "components/layouts/appbarForm"

import { getStores } from "api/api.stores"
import { getModels } from "api/api.models"
export default function Index({ params }) {
  const { pid } = params

  const {
    data: STORES,
    isFetching: isFetchingStores,
    isError: isErrorStores,
    error: errorStores,
  } = useQuery("allStores", () => getStores(0, 0, pid, ["name", "description", "project"]), { retry: false })

  const {
    data: MODELS,
    isFetching: isFetchinggModels,
    isError: isErrorModels,
    error: errorModels,
  } = useQuery("allModels", () => getModels(0, 0), { retry: false })

  if (errorStores?.response.status === 401 || errorModels?.response.status === 401) Router.push("/login")

  const isFetching = isFetchingStores || isFetchinggModels
  const isError = isErrorStores || isErrorModels

  return (
    <Appbar>
      <Form STORES={STORES} MODELS={MODELS} isFetching={isFetching} isError={isError} pid={pid} />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params } }
}

Index.auth = { role: "admin" }
