from agilkia import TraceSet,Trace
import numpy as np
import json
import datetime
import copy
def load_from_dict(cls,data) -> 'TraceSet':
    """Load traces from the given file.
    This upgrades older trace sets to the current version if possible.
    """
    if isinstance(data, dict) and data.get("__class__", None) == "TraceSet":
        return cls.upgrade_json_data(data)
    else:
        raise Exception("unknown JSON file format: " + str(data)[0:60])


def prepare_dataset(execution_traces_dict,test_traces_dict):
    tests_not_empty=True
    if len(test_traces_dict.keys()) == 0:
        tests_not_empty=False
    print(execution_traces_dict['__class__'])
    #Import of execution traces and test traces into a TraceSet Object
    traceset_execution = load_from_dict(TraceSet,execution_traces_dict)

    if tests_not_empty:
        traceset_tests=load_from_dict(TraceSet,test_traces_dict)
    else:
        traceset_tests=TraceSet(traces=[])

    #Merging the execution traces and test traces into a single TracesetObjet
    traceset_global=copy.deepcopy(traceset_execution)
    traceset_global.extend(traceset_tests.traces)

    #Compute bag of words representation of the three various traceSet

    all_actions=traceset_global.get_all_actions()

    global_data = traceset_global.get_trace_data(method="action_counts",columns=all_actions)
    execution_data = traceset_execution.get_trace_data(method="action_counts",columns=all_actions)
    test_data = traceset_tests.get_trace_data(method="action_counts",columns=all_actions)

    # check that names of columns are the same (but status values can be different)
    if not all([c1[:5] == c2[:5] == c3[:5]for (c1, c2,c3) in zip(execution_data.columns, test_data.columns,global_data.columns) if c2 != "none"]):
        raise ValueError("ERROR: test columns are not the same as data.columns.")

    return traceset_global,global_data,execution_data,test_data

def dataframe_to_contiguous(data):
    data=copy.deepcopy(data)
    new_numpy=data.to_numpy()
    return np.ascontiguousarray(new_numpy,dtype=new_numpy.dtype)