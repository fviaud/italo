import React from "react"
import { useQuery } from "react-query"

import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"

import Stores from "components/stores/items"
import Appbar from "components/layouts/appbar"

import { getStores } from "api/api.stores"

import Link from "next/link"

export default function Index({ query }) {
  const page = query.page
  const fetchObjects = (page = 1) => getStores(page, 10)
  const {
    isLoading,
    isError,
    data: STORES,
    refetch,
  } = useQuery(["traces", page], () => fetchObjects(page), { keepPreviousData: true })

  return (
    <Appbar>
      <Box sx={{ display: "flex", justifyContent: "space-between" }} mt={2}>
        <Typography variant="h4" color="primary">
          Traces
        </Typography>
        <Link href={"/traces/new/"} style>
          <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
            Add
          </Button>
        </Link>
      </Box>
      <Divider />
      <Stores STORES={STORES} page={page} isLoading={isLoading} isError={isError} refetch={refetch} />
    </Appbar>
  )
}
export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

Index.auth = { role: "admin" }
