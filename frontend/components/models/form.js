import React from "react"
import Router from "next/router"
import { useQueryClient } from "react-query"

import TextField from "@mui/material/TextField"
import Box from "@mui/material/Box"
import { LoadingButton } from "@mui/lab"
import Stack from "@mui/material/Stack"
import IconButton from "@mui/material/IconButton"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import Divider from "@mui/material/Divider"
import Grid from "@mui/material/Grid"

import { Formik, Form } from "formik"
import * as Yup from "yup"
import { addModels, updateModels } from "api/api.models"
import { notifMessage } from "components/utils/notif"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

function FormModel({ model, isFetching, isError }) {
  const queryClient = useQueryClient()

  if (isFetching) return <Spinner />
  if (isError) return <Error />

  const curentUser = queryClient.getQueryData("user")
  const initialValues = {
    name: model?.name ?? "",
    description: model?.description ?? "",
  }
  const validationSchema = Yup.object({
    name: Yup.string().required("Is required"),
    description: Yup.string().required("Is required"),
  })

  const onSubmit = async (values) => {
    const formData = new FormData()
    if (model) {
      formData.append("id", model._id.$oid)
    }
    formData.append("name", values.name)
    formData.append("author", curentUser.data.name)
    formData.append("description", values.description)
    try {
      ;(await model) ? updateModels(formData) : addModels(formData)
      notifMessage("success", "Add with success !")
      Router.back()
    } catch (error) {
      notifMessage("error", "Failed to add !")
    }
  }

  return (
    <Stack mt={2}>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Stack direction="row" spacing={2}>
          <IconButton edge="end" onClick={() => Router.back()}>
            <ArrowBackIcon />
          </IconButton>
        </Stack>
      </Box>
      <Divider />
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {(formik) => (
          <Form autoComplete="off" noValidate>
            <Stack spacing={2} sx={{ mt: 2 }}>
              <TextField
                margin="dense"
                id="name"
                label="Name"
                fullWidth
                variant="outlined"
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={Boolean(formik.touched.name && formik.errors.name)}
                helperText={formik.touched.name && formik.errors.name}
              />
              <TextField
                id="description"
                name="description"
                label="Description"
                fullWidth
                multiline
                maxRows={4}
                required
                value={formik.values.description}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={Boolean(formik.touched.description && formik.errors.description)}
                helperText={formik.touched.description && formik.errors.description}
              />
              <Grid container spacing={0} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={3}>
                  <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                    {model ? "update" : "Submit"}
                  </LoadingButton>
                </Grid>
              </Grid>
            </Stack>
          </Form>
        )}
      </Formik>
    </Stack>
  )
}

export default FormModel
