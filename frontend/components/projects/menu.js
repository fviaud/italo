import * as React from "react"
import Box from "@mui/material/Box"
import Tab from "@mui/material/Tab"
import TabContext from "@mui/lab/TabContext"
import TabList from "@mui/lab/TabList"
import TabPanel from "@mui/lab/TabPanel"
import Chip from "@mui/material/Chip"
import Typography from "@mui/material/Typography"
import Grid from "@mui/material/Grid"

import Chart from "components/charts"

export default function LabTabs({ project }) {
  const [value, setValue] = React.useState(project.models[0].name)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  return (
    <Grid container columns={{ md: 12 }}>
      <Grid item xs={6}>
        <Typography variant="h5">Clustering</Typography>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
          eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo
          lobortis eget.
        </Typography>
      </Grid>
      <Grid item xs={6}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList onChange={handleChange} aria-label="lab API tabs example">
              {project.models.map((item, index) => (
                <Tab
                  style={{ textTransform: "none" }}
                  key={index}
                  label={
                    <div>
                      {item.name}
                      <Chip size="small" label={Math.round(item.results.score * 100)} />
                    </div>
                  }
                  value={item.name}
                />
              ))}
            </TabList>
          </Box>
          {project.models.map((item, index) => (
            <TabPanel value={item.name} key={index}>
              <Chart project={project} model={item} indexModel={index} />
            </TabPanel>
          ))}
        </TabContext>
      </Grid>
    </Grid>

    // </Box>
  )
}
