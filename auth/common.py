from flask import Flask, session, redirect,abort,jsonify,make_response, redirect
import flask_login

from providers.model import Users

def common_routes(app):

    login_manager = flask_login.LoginManager()
    login_manager.init_app(app)

    # @login_manager.user_loader
    # def user_loader(email):
    #     return Users.objects(email=email).first()
    
    @login_manager.user_loader
    def user_loader(id):
        return Users.objects(pk=id)[0]
        
    @app.route('/api/auth/signout')
    def logout():
        session.clear()
        return redirect('/')
    
    @app.route('/api/auth/session')
    @flask_login.login_required
    def protected_area():
        if "_user_id" in session:
            response = make_response(jsonify(dict(session)),200)
            response.headers['X-Forwarded-User'] = session['_user_id']
            return response
        else:
            return abort(401)
        