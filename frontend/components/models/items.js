import * as React from "react"
import Router, { useRouter } from "next/router"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import IconButton from "@mui/material/IconButton"
import DeleteIcon from "@mui/icons-material/Delete"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Pagination from "@mui/material/Pagination"
import ModelTrainingIcon from "@mui/icons-material/ModelTraining"

import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { delModels } from "api/api.models"
import { notifMessage } from "components/utils/notif"

export default function Items({ MODELS, isLoading, page = 0, isError, refetch }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />

  return (
    <Stack mt={2}>
      {MODELS.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {MODELS.data.results.map((model, index) => (
          <ListItemButton dense disableRipple key={index} onClick={() => Router.push(`/models/edit/${model._id.$oid}`)}>
            <ListItem
              secondaryAction={
                <IconButton
                  edge="end"
                  onClick={async (e) => {
                    e.stopPropagation()
                    if (window.confirm("Do you wand delete this object ?")) {
                      try {
                        await delModels(model._id.$oid)
                        refetch()
                        notifMessage("success", `Delete with success !`)
                      } catch (error) {
                        notifMessage("error", "Failed to delete !")
                      }
                    }
                  }}>
                  <DeleteIcon />
                </IconButton>
              }>
              <ListItemAvatar>
                <Avatar>
                  <ModelTrainingIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={model.name} secondary={<span>{model.description}</span>} />
            </ListItem>
          </ListItemButton>
        ))}
      </List>
      {(MODELS.data.totalPages > 1 || MODELS.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={MODELS.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/models/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
