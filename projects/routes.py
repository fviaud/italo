from flask import request, jsonify
from bson import ObjectId
from mongoengine.queryset.visitor import Q
import json
from math import *

from model import Projects,Jobs

def api_routes(app):
    @app.route('/api/v0/projects', methods=['GET'])
    def projects():
        userId = request.headers.get("X-Forwarded-User")
        page = request.args.get('page', 1, type=int)
        limit = request.args.get('limit', 10, type=int)
        count = Projects.objects().count()
        totalPages = 0 if limit <= 0 else ceil(count/limit)
        projects = Projects.objects().skip((page-1)*limit).limit(limit)
        return jsonify(
            {
                "page": page,
                "results": list(projects),
                "totalPages": totalPages,
                "documents": count
            }
        ), 200
    
    @app.route('/api/v0/projects/<pid>/jobs', methods=['GET'])
    def jobs(pid):
        userId = request.headers.get("X-Forwarded-User")
        page = request.args.get('page', 1, type=int)
        limit = request.args.get('limit', 10, type=int)
        totalPages = 0 if limit <= 0 else ceil(Jobs.objects(project=pid).count()/limit)
        fields = request.args.getlist('fields[]', type=str)
        if len(fields) > 0 and '_id' not in fields:
            jobs = Jobs.objects(project=pid).only(*fields).skip((page-1)*limit).limit(limit)
        else:
            jobs = Jobs.objects(project=pid).skip((page-1)*limit).limit(limit)
        
        return jsonify(
            {
                "page": page,
                "results": list(jobs),
                "totalPages": totalPages,
                "documents": Jobs.objects().count()
            }
        ), 200

    @app.route('/api/v0/projects/<id>', methods=['GET'])
    def project(id):
        try:
            userId = request.headers.get("X-Forwarded-User")
            project = Projects.objects(pk=id)
            return jsonify(project), 200
        except KeyError:
            return 'Erreur acces api project', 400
    
    @app.route('/api/v0/projects/jobs/<id>', methods=['GET'])
    def job(id):
        try:
            userId = request.headers.get("X-Forwarded-User")
            job = Jobs.objects(pk=id)
            return jsonify(job), 200
        except KeyError:
            return 'Erreur acces api job', 400


    @app.route('/api/v0/projects', methods=['POST'])
    def create_project():
        try:
            new_project = Projects(
                name=request.form['name'],
                description=request.form['description'],
                author=request.form['author'],
            )
            new_project.save()
            return jsonify(new_project.to_json()), 200
        # except NotUniqueError:
        #     return 'User Already Exists', 400
        # except ValidationError:
        #     return 'Provide an Email and Password valid in the request body', 400
        except KeyError:
            return 'Format not bvalid in the request body', 400

    @app.route('/api/v0/projects/jobs', methods=['POST'])
    def create_job():
        try:
            new_job = Jobs(
                name=request.form['name'],
                description=request.form['description'],
                project=request.form['project'],
                execution_traces=json.loads(request.form['execution_traces']),
                test_traces=json.loads(request.form['test_traces']),
                models=json.loads(request.form['models']),
                generativeModels=json.loads(request.form['generativeModels']),
                nbScenarios=request.form['nbScenarios'],
                status=request.form['status'],
                author=request.form['author'],
            )
            new_job.save()
            return jsonify(new_job.to_json()), 200
        # except NotUniqueError:
        #     return 'User Already Exists', 400
        # except ValidationError:
        #     return 'Provide an Email and Password valid in the request body', 400
        except KeyError:
            return 'Format not bvalid in the request body', 400
    
    @app.route('/api/v0/projects/jobs', methods=['PATCH'])
    def update_job():
        try:
            job = Jobs.objects(pk=request.form['id']).update(
                set__name=request.form['name'],
                set__description=request.form['description'],
                set__execution_traces=json.loads(request.form['execution_traces']),
                set__test_traces=json.loads(request.form['test_traces']),
                set__models=json.loads(request.form['models']),
                set__generativeModels=json.loads(
                    request.form['generativeModels']),
                set__nbScenarios=request.form['nbScenarios'],
                set__status=""
            )
            return jsonify({"update": "ok"}), 200
        # except NotUniqueError:
        #     return 'User Already Exists', 400
        # except ValidationError:
        #     return 'Provide an Email and Password valid in the request body', 400
        except KeyError:
            return 'Format not bvalid in the request body', 400

    @app.route('/api/v0/projects', methods=['PATCH'])
    def update_one_project():
        try:
            userId = request.headers.get("X-Forwarded-User")
            project = Projects.objects(Q(pk=request.form['id']) & (Q(public=True) | Q(author=userId))).update(
                set__name=request.form['name'],
                set__description=request.form['description'],
                # set__dataset=json.loads(request.form['dataset']),
                # set__models=json.loads(request.form['models']),
                # set__generativeModels=json.loads(
                #     request.form['generativeModels']),
                # set__nbScenarios=request.form['nbScenarios'],
                # set__public=json.loads(request.form['public']),
                # set__status=""
                )
            return jsonify({"update": "ok"}), 200
        except KeyError:
            return 'Format not bvalid in the request body', 400

    # @app.route('/api/v0/projects/<id>/status', methods=['PATCH'])
    # def update_status_project(id):
    #     try:
    #         userId = request.headers.get("X-Forwarded-User")
    #         Projects.objects(Q(pk=id) & (Q(public=True) | Q(author=userId))).update(
    #             set__status=request.form.get('status'))
    #         Projects.objects(Q(pk=id) & (Q(public=True) | Q(author=userId))).update(
    #             set__status=request.form.get('status'))
    #         return jsonify({"update": "ok"}), 200
    #     except:
    #         return 'Format not bvalid in the request body', 400
    
    @app.route('/api/v0/projects/jobs/<id>/status', methods=['PATCH'])
    def update_status_project(id):
        try:
            userId = request.headers.get("X-Forwarded-User")
            Jobs.objects(pk=id).update(set__status=request.form.get('status'))
            return jsonify({"update": "ok"}), 200
        except:
            return 'Format not bvalid in the request body', 400

    @app.route('/api/v0/projects/<id>', methods=['DELETE'])
    def delete_project(id):
        try:
            userId = request.headers.get("X-Forwarded-User")
            project = Projects.objects(pk=id)
            project.delete()
            return jsonify(project.to_json()), 200
        except KeyError:
            return 'failed to delete project', 400

    @app.route('/api/v0/projects/jobs/<id>', methods=['DELETE'])
    def delete_job(id):
        try:
            userId = request.headers.get("X-Forwarded-User")
            job = Jobs.objects(pk=id)
            job.delete()
            return jsonify(job.to_json()), 200
        except KeyError:
            return 'failed to delete job', 400
