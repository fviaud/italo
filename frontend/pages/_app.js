import React, { useEffect } from "react"
import Router from "next/router"
import { useQuery, QueryClient, QueryClientProvider } from "react-query"

import { ToastContainer } from "react-toastify"

import "react-toastify/dist/ReactToastify.css"

import { ThemeProvider } from "@mui/material/styles"
import theme from "components/themes/orange"
import Spinner from "components/utils/spinner"
import { getSession } from "api/api.auth"

const queryClient = new QueryClient()

export default function App({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <QueryClientProvider client={queryClient}>
        {Component.auth ? (
          <Auth>
            <Component {...pageProps} />
          </Auth>
        ) : (
          <Component {...pageProps} />
        )}
        <ToastContainer
          position="bottom-right"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      </QueryClientProvider>
    </ThemeProvider>
  )
}

function Auth({ children }) {
  const { isLoading, data: SESSION } = useQuery("session", () => getSession(), { retry: false })

  if (isLoading) return <Spinner />
  if (SESSION?.data) {
    return children
  } else {
    Router.push("/login")
  }

  return <Spinner />
}
