import axios from "axios"

const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_TRACES}`,
  withCredentials: true,
})

export const getTraces = (page, limit = 0) => {
  return api.get(`traces?page=${page}&limit=${limit}`)
}

export const getTrace = (id) => {
  return api.get(`traces/${id}`)
}

export const addTraces = (object) => {
  return api.post("traces/file", object)
}

export const updateTraces = (object) => {
  return api.patch("traces", object)
}

export const delTraces = (id) => {
  return api.delete(`traces/${id}`)
}

export const convertTraces = (id) => {
  return api.get(`traces/convert/${id}`)
}

export const getClusters = (idjob, idmodel, idcluster, page = 1, limit = 10) => {
  return api.get(`traces/job/${idjob}`, { params: { idmodel, idcluster, page, limit } })
}

export const getTestClusters = (idjob, idmodel, idcluster, page = 1, limit = 10) => {
  return api.get(`traces/job/${idjob}/tests`, { params: { idmodel, idcluster, page, limit } })
}
