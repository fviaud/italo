import * as React from "react"
import Box from "@mui/material/Box"
import Stepper from "@mui/material/Stepper"
import Step from "@mui/material/Step"
import StepLabel from "@mui/material/StepLabel"

export default function HorizontalLabelPositionBelowStepper({ steps }) {
  console.log(steps)
  return (
    <Box sx={{ width: "100%" }}>
      <Stepper activeStep={1} alternativeLabel>
        {steps.map((label) => (
          <Step key={label.action}>
            <StepLabel>{label.action}</StepLabel>
          </Step>
        ))}
      </Stepper>
    </Box>
  )
}
