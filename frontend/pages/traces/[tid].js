import React from "react"
import { useQuery } from "react-query"

import Trace from "components/traces/item"
import Appbar from "components/layouts/appbar"
import { getTrace } from "api/api.traces"

export default function Index({ params }) {
  const { tid } = params
  const { data: TRACE, isLoading, isFetching, isError } = useQuery("trace", () => getTrace(tid), { retry: false })
  return (
    <Appbar>
      <Trace trace={TRACE?.data[0]} isError={isError} isFetching={isFetching} />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params } }
}

Index.auth = { role: "admin" }
