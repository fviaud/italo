import React from "react"
import Router from "next/router"
import { useQueryClient } from "react-query"

import TextField from "@mui/material/TextField"
import Box from "@mui/material/Box"
import { LoadingButton } from "@mui/lab"
import Stack from "@mui/material/Stack"
import MenuItem from "@mui/material/MenuItem"
import Typography from "@mui/material/Typography"
import Paper from "@mui/material/Paper"
import FormLabel from "@mui/material/FormLabel"
import Button from "@mui/material/Button"

import { Formik, Form } from "formik"
import * as Yup from "yup"

import { notifMessage } from "components/utils/notif"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { addTraces } from "api/api.traces"

function FormModel({ trace, isFetching, isError, pid }) {
  const queryClient = useQueryClient()
  const curentSession = queryClient.getQueryData("session")

  if (isFetching) return <Spinner />
  if (isError) return <Error />

  const initialValues = {
    name: trace?.name ?? "",
    description: trace?.description ?? "",
    type_trace: "",
    file: null,
  }
  const validationSchema = Yup.object({
    name: Yup.string().required("Is required"),
    // description: Yup.string().required("Is required"),
    type_trace: Yup.string().required("Is required"),
    file: Yup.mixed().test("required", "You need to provide a file", (file) => {
      // return file && file.size <-- u can use this if you don't want to allow empty files to be uploaded;
      if (file) return true
      return false
    }),
    // .test("fileSize", "The file is too large", (file) => {
    //   //if u want to allow only certain file sizes
    //   return file && file.size <= 2000000000
    // }),
  })

  const onSubmit = async (values) => {
    const formData = new FormData()
    formData.append("name", values.name)
    formData.append("description", values.description)
    formData.append("type_trace", values.type_trace)
    formData.append("trace", file.files[0])
    formData.append("author", curentSession.data._user_id)
    formData.append("project", pid)
    try {
      await addTraces(formData)
      // setUpLoading(false)
      notifMessage("success", "Add trace with success !")
      Router.back()
      // refetch()
    } catch (error) {
      notifMessage("error", "Failed to add trace !")
      // setUpLoading(false)
      // refetch()
    }
  }

  return (
    <Stack mt={2}>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Stack direction="row" spacing={2} alignItems="center">
          <Typography variant="h6" color="primary">
            {trace ? "Modify trace" : "New trace"}
          </Typography>
        </Stack>
      </Box>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {(formik) => (
          <Form autoComplete="off" noValidate>
            <Stack spacing={2} sx={{ mt: 2 }}>
              <Paper variant="outlined">
                <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                  <FormLabel>
                    <Typography variant="h6" style={{ fontWeight: 600 }}>
                      Naming, visibility
                    </Typography>
                  </FormLabel>
                </Box>
                <Box sx={{ p: 2, paddingTop: 0 }}>
                  <Stack spacing={2} sx={{ mt: 2 }}>
                    <TextField
                      margin="dense"
                      id="name"
                      label="Name"
                      fullWidth
                      variant="outlined"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.name && formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                    <TextField
                      id="description"
                      name="description"
                      label="Description (optional)"
                      fullWidth
                      multiline
                      maxRows={4}
                      // required
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.description && formik.errors.description)}
                      helperText={formik.touched.description && formik.errors.description}
                    />
                  </Stack>
                </Box>
              </Paper>

              <Paper variant="outlined">
                <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                  <FormLabel>
                    <Typography variant="h6" style={{ fontWeight: 600 }}>
                      File
                    </Typography>
                  </FormLabel>
                </Box>
                <Box sx={{ p: 2, paddingTop: 0 }}>
                  <Stack spacing={2} sx={{ mt: 2 }}>
                    <TextField
                      id="type_trace"
                      name="type_trace"
                      label="Type of trace"
                      select
                      required
                      value={formik.values.type_trace}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      fullWidth
                      error={Boolean(formik.touched.type_trace && formik.errors.type_trace)}
                      helperText={formik.touched.type_trace && formik.errors.type_trace}>
                      {["Scanette", "Teaming", "Json file"].map((value, index) => (
                        <MenuItem key={index} value={value}>
                          {value}
                        </MenuItem>
                      ))}
                    </TextField>

                    <TextField
                      id="file"
                      name="file"
                      type="file"
                      required
                      value={formik.values.file}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      fullWidth
                      error={Boolean(formik.touched.file && formik.errors.file)}
                      helperText={formik.touched.file && formik.errors.file}
                    />
                  </Stack>
                </Box>
              </Paper>
              {/* <Grid container spacing={0} direction="column" alignItems="center" justifyContent="center">
                <Grid item xs={3}>
                  <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                    {trace ? "update" : "Submit"}
                  </LoadingButton>
                </Grid>
              </Grid> */}
              <Stack spacing={2} sx={{ pt: 2 }} direction="row">
                <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                  {trace ? "update" : "Submit"}
                </LoadingButton>
                <Button color="primary" onClick={() => Router.back()}>
                  Cancel
                </Button>
              </Stack>
            </Stack>
          </Form>
        )}
      </Formik>
    </Stack>
  )
}

export default FormModel
