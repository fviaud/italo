import React from "react"
import Router, { useRouter } from "next/router"
import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"
import Pagination from "@mui/material/Pagination"
import Chip from "@mui/material/Chip"
import Avatar from "@mui/material/Avatar"
import ArrowForwardIcon from "@mui/icons-material/ArrowForward"

const pageSize = 10

const removeDuplicateEvent = (trace) => {
  let lastEvent = { action: "" };
  trace.forEach(event => {
    event.counter = 1;
  });
  const finalTrace = trace.filter((event) => {
    if (event.action !== lastEvent.action) {
      lastEvent = event
      return true
    } else {
      lastEvent.counter += 1
      return false
    }
  })
  return finalTrace;
}

const stringToColor = (str, saturation = 50, ligthness = 70) => {
  var hash = 0
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }
  var h = (hash + str.length) % 360
  return "hsl(" + h + ", " + saturation + "%, " + ligthness + "%)"
}

function Details({ CLUSTER, pid, jid, query, page = 1, menu }) {
  const { idmodel, idcluster } = query
  const router = useRouter()
  return (
    <>
      {Array.isArray(CLUSTER.data.results) && CLUSTER.data.results.length > 0 ?
        <>
          <List dense>
            {CLUSTER.data.results.map((trace, index) => (
              <React.Fragment key={index}>
                {index !== 0 && <Divider />}
                <ListItem key={index}>
                  {/* <Stepper steps={trace} /> */}
                  <Box display="flex" sx={{ alignItems: "center", flexWrap: "wrap" }}>
                    <Typography sx={{ marginRight: 1 }}>{`${trace.counter} x `}</Typography>
                    {removeDuplicateEvent(trace.events).map((event, indexBis) => {
                      if (trace.events[indexBis + 1] !== event.action) {
                        return (
                          <React.Fragment key={indexBis}>
                            {indexBis !== 0 && <ArrowForwardIcon />}
                            <Chip
                              sx={{ m: 1, bgcolor: stringToColor(event.action) }}
                              label={event.action}
                              size="small"
                              variant="outlined"
                              avatar={event.counter > 1 ? <Avatar style={{ color: "black" }}>{Number.parseInt(event.counter)}</Avatar> : <></>}
                            />
                          </React.Fragment>
                        )
                      }
                    })}
                  </Box>
                </ListItem>
              </React.Fragment>
            ))}
          </List>
          {(CLUSTER.data.totalPages > 1 || CLUSTER.data.totalPages < page) && (
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                p: 1,
                m: 1,
              }}>
              <Pagination
                count={CLUSTER.data.totalPages}
                page={parseInt(router.query.page) || 1}
                onChange={(event, value) =>
                  // Router.push(`/projects/traces?idjob=${idjob}&idmodel=${idmodel}&idcluster=${idcluster}&menu=${menu}&page=${value}`)

                  Router.push(
                    `/projects/${pid}/jobs/${jid}/details?idmodel=${idmodel}&idcluster=${idcluster}&menu=${menu}&page=${value}`
                  )
                }
              />
            </Box>
          )}
        </> :
        <Typography>No execution traces for this cluster</Typography>
      }
    </>
  )
}

export default Details
