import React from "react"
import { useQueryClient } from "react-query"

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

import Chart from "./charts"

function Synthese({ idcluster, isLoading, isError, job, query }) {
  // const queryClient = useQueryClient()
  // const job = queryClient.getQueryData("job")
  const { idmodel } = query
  const cluster = `cluster${idcluster}`

  if (isLoading) return <Spinner />
  if (isError) return <Error />

  return (
    <Chart
      cluster_global={job.data[0].models[idmodel].results.events_distribution[cluster].global}
      cluster={job.data[0].models[idmodel].results.events_distribution[cluster].current}
    />
  )
}

export default Synthese
