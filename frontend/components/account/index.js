import React from 'react'
import Link from 'next/link'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import DeleteIcon from '@mui/icons-material/Delete'
import Button from '@mui/material/Button'
import Typography from '@mui/material/Typography'
import Stack from '@mui/material/Stack'
import Divider from '@mui/material/Divider'

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

function item({ user, isLoading, isError }) {
    if (isLoading) return <Spinner />
    if (isError) return <Error />
    return (
        <Stack mt={2}>
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }} >
                <Typography variant="h4" color="primary" >Account</Typography>
                <Link href={"/models/new/"} style><Button variant="text" startIcon={<DeleteIcon />} >Delete my account</Button></Link>
            </Box>
            <Divider />
            <List dense>
                <ListItem >
                    <ListItemText
                        primary={<Typography color="secondary">Name</Typography>}
                        secondary={user.name}
                    />
                </ListItem>
                <ListItem >
                    <ListItemText
                        primary={<Typography color="secondary">Email</Typography>}
                        secondary={user.email}
                    />
                </ListItem>

            </List>
        </Stack>
    )
}

export default item