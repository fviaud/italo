import * as React from "react"
import Link from "next/link"
import { useRouter } from "next/router"

import Box from "@mui/material/Box"
import Tabs from "@mui/material/Tabs"
import Tab from "@mui/material/Tab"
import Divider from "@mui/material/Divider"
import { Stack } from "@mui/system"

function LinkTab(props) {
  return (
    <Link href={props.href}>
      <Tab component="a" {...props} sx={{ textTransform: "none" }} />
    </Link>
  )
}

export default function NavTabs({ children, id }) {
  const router = useRouter()

  const items = [
    { name: "Traces", path: `/projects/${id}/traces` },
    { name: "Jobs", path: `/projects/${id}/jobs` },
    { name: "Members", path: `/projects/${id}/members` },
    { name: "Settings", path: `/projects/${id}/settings` },
  ]

  const [value, setValue] = React.useState(items.findIndex((item) => item.path === router.asPath) || 0)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <Stack>
      <Box sx={{ width: "100%" }}>
        <Tabs value={value} onChange={handleChange} aria-label="nav tabs example">
          {items.map((item, index) => (
            <LinkTab key={index} label={item.name} href={item.path} />
          ))}
        </Tabs>
      </Box>
      <Divider />
      <main>{children}</main>
    </Stack>
  )
}
