import json
from pymongo import MongoClient
import os

def get_db(mode,collection_name):
    if mode =='SERVER' or mode=='SERVER_DEBUG':
        client=MongoClient(os.environ.get('MONGODB_URI'))
        db=client[os.environ.get('DATABASE')]
        collection=db[collection_name]
        collection_datasets=db['datasets']
        collection_stores=db['stores']
        collection_traces=db['traces']
        collection_file=db['fs.files']
        return db, collection,collection_traces,collection_file,collection_datasets,collection_stores
    elif mode =='LOCAL_DEBUG':
        return None,None,None,None,None,None
    else:
        raise ValueError("mode should be SERVER or LOCAL")


def read_db(mode,collection):
    if mode=='SERVER' or mode=='SERVER_DEBUG':
        return collection.find({"status": "in progress"})
    elif mode=='LOCAL_DEBUG':
        with open('./local_data/local_data.json') as json_file:
             return [dict(json.load(json_file))]

def update_db_clustering(mode,collection,item,model,clustering_results):
    if mode =='SERVER' or mode=='SERVER_DEBUG':
        project = collection.update_one(
            {"_id": item["_id"], "models.name": model["name"]},
            {"$set": {
                "models.$.results": {
                    "score": clustering_results["score"],
                    "execution_traces_distribution": clustering_results["execution_traces_distribution"],
                    "test_traces_distribution": clustering_results["test_traces_distribution"],
                    "execution_clusters": clustering_results["execution_clusters"],
                    "test_clusters": clustering_results["test_clusters"],
                    "events_distribution": clustering_results["events_distribution"],
                    "clustering_model_serialized": clustering_results["clustering_model_serialized"],
                    "normalizer_model_serialized": clustering_results["normalizer_model_serialized"]
                }
            }
            })
        return project

    elif mode =='LOCAL_DEBUG':
        print(clustering_results)

        return None
    else:
        raise ValueError("mode should be SERVER or LOCAL")

def update_db_generation(mode,collection,item,generative_model,generative_results):
    if mode =='SERVER' or mode =='SERVER_DEBUG':
        project = collection.update_one(
            {"_id": item["_id"],"generativeModels.name": generative_model["name"]},
            {"$set": {
                "generativeModels.$.results": {
                    "tests_per_clusters" : generative_results["tests_per_clusters"],
                    "usage":generative_results['usage'],
                    "diversity":generative_results['diversity'],
                    "execution_time":generative_results['execution_time']
                }
            }
            })
        return project

    elif mode =='LOCAL_DEBUG':
        print(generative_results)
        return None
    else:
        raise ValueError("mode should be SERVER or LOCAL")

def update_db_change_status_project(mode,collection,item,status):
    if mode =='SERVER' or mode=='SERVER_DEBUG':
        print(status)
        project = collection.update_one(
            {"_id": item["_id"]},
            {"$set": {
                "status": status
            }
            })
        return project

    elif mode =='LOCAL_DEBUG':
        print(status)
        return None
    else:
        raise ValueError("mode should be SERVER or LOCAL")