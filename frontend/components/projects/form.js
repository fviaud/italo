import React from "react"
import { useQueryClient } from "react-query"
import Router from "next/router"
import { Formik, Form } from "formik"
import * as Yup from "yup"

import Box from "@mui/material/Box"
import TextField from "@mui/material/TextField"
import { FormLabel } from "@mui/material"
import Stack from "@mui/material/Stack"
import { LoadingButton } from "@mui/lab"
import Typography from "@mui/material/Typography"
import Paper from "@mui/material/Paper"
import Button from "@mui/material/Button"

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"

import { addProjects, updateProjects } from "api/api.projects"

function FormProject({ project, isFetching, isError }) {
  const queryClient = useQueryClient()
  if (isFetching) return <Spinner />
  if (isError) return <Error />

  const curentSession = queryClient.getQueryData("session")

  const initialValues = {
    name: project?.name ?? "",
    description: project?.description ?? "",
  }
  const validationSchema = Yup.object({
    name: Yup.string().required("Is required"),
    // description: Yup.string().required("Is required"),
  })
  const onSubmit = async (values) => {
    const formData = new FormData()
    if (project) {
      formData.append("id", project._id.$oid)
    }
    formData.append("author", curentSession.data._user_id)
    formData.append("name", values.name)
    formData.append("description", values.description)

    try {
      ;(await project?.name) ? updateProjects(formData) : addProjects(formData)
      notifMessage("success", `${project?.name ? "Update " : "Add "}project with success !`)
    } catch (error) {
      notifMessage("error", "Failed add or update project!")
    }
    Router.back()
  }

  return (
    <Stack mt={2}>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Stack direction="row" spacing={2} alignItems="center">
          <Typography variant="h6" color="primary">
            {project ? "Modify project" : "New project"}
          </Typography>
        </Stack>
      </Box>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {(formik) => (
          <Form autoComplete="off" noValidate>
            <Stack spacing={2} sx={{ pt: 2 }}>
              <Paper variant="outlined">
                <Stack>
                  <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                    <FormLabel>
                      <Typography variant="h6" style={{ fontWeight: 600 }}>
                        Naming, visibility
                      </Typography>
                    </FormLabel>
                  </Box>
                  <Box sx={{ p: 2 }}>
                    <TextField
                      id="name"
                      name="name"
                      label="Name"
                      fullWidth
                      required
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.name && formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                  </Box>
                  <Box sx={{ p: 2 }}>
                    <TextField
                      id="description"
                      name="description"
                      label="Description (optional)"
                      fullWidth
                      multiline
                      maxRows={4}
                      // required
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.description && formik.errors.description)}
                      helperText={formik.touched.description && formik.errors.description}
                    />
                  </Box>
                </Stack>
              </Paper>
              <Stack spacing={2} sx={{ pt: 2 }} direction="row">
                <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                  {project ? "update" : "Submit"}
                </LoadingButton>
                <Button color="primary" onClick={() => Router.back()}>
                  Cancel
                </Button>
              </Stack>
            </Stack>
          </Form>
        )}
      </Formik>
    </Stack>
  )
}

export default FormProject
