from flask import redirect,request
from flask_login import login_user
from requests.auth import HTTPBasicAuth

from .model import Users
import requests
import urllib
import os
import json

def orange_routes(app):
    

    @app.route('/api/auth/login/orange')
    def login_orange():
        urlOidc = f"{os.environ.get('ORANGE_URI')}/authorize?response_type=code&client_id={os.environ.get('ORANGE_CLIENT_ID')}&scope=openid%20email%20profile&redirect_uri={os.environ.get('ORANGE_REDIRECT_URI')}&state=upToYouData"
        return redirect(urlOidc)

    @app.route('/api/auth/callback/orange')
    def auth_orange():
        details = {
        'grant_type': 'authorization_code',
        'code': request.args.get('code',0, type=str) ,
        'redirect_uri': os.environ.get('ORANGE_REDIRECT_URI')
        } 

        formBody=[]
        for key, valeur in details.items() : 
            encodedKey = urllib.parse.quote(key)
            encodedValue = urllib.parse.quote(valeur)
            formBody.append(encodedKey + "=" + encodedValue)

        req = requests.post(
            os.environ.get('ORANGE_URI')+"/token",
            headers={'Content-type': 'application/x-www-form-urlencoded'},
            auth=HTTPBasicAuth(os.environ.get('ORANGE_CLIENT_ID'), os.environ.get('ORANGE_CLIENT_SECRET')),
            data='&'.join(formBody))
        
        userinfo = requests.get(
            os.environ.get("ORANGE_URI")+'/userinfo', 
            headers= {'Authorization': 'Bearer ' + json.loads(req.content)["access_token"]})

        if userinfo:
            emails= [
                "fviaud.ext@orange.com",
                "adrien.nogues.ext@orange.com",
                "frederic.tamagnan@orange.com",
                "yannick.dubucq@orange.com",
                "franck.mhamed@orange.com",
                "guillaume.yomo@orange.com",
                "frederic.mazeiras@orange.com"
                ]
            userinfo=json.loads(userinfo.content)
            if userinfo['email'] in emails :
                user=Users.objects(email=userinfo['email']).first()
                if not user:
                    user = Users(
                        email=userinfo['email'],
                        name=userinfo['name'])
                    user.save()
                user.id = user.to_json()["id"]
                login_user(user)


        return redirect('/')