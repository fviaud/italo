import React from 'react'
import Model from "components/models/form"
import Appbar from "components/layouts/appbar"
export default function Index() {
    return <Appbar><Model /></Appbar>
}
Index.auth = { role: "admin" }


