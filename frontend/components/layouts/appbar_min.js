import * as React from "react"
import AppBar from "@mui/material/AppBar"
import Toolbar from "@mui/material/Toolbar"
import Typography from "@mui/material/Typography"
import Container from "@mui/material/Container"
import Stack from "@mui/material/Stack"
import Box from "@mui/material/Box"
import Footer from "components/footer"
import GlobalStyles from "@mui/material/GlobalStyles"
import Image from "next/image"
import img from "public/ia.png"
import imgOrange from "public/favicon.ico"

const ResponsiveAppBar = ({ children }) => {
  return (
    <>
      <GlobalStyles
        styles={{
          body: {
            margin: 0,
            height: "100vh",
          },
          "#__next": {
            height: "100%",
            display: "flex",
            flexDirection: "column",
          },
        }}
      />
      <AppBar>
        <Container>
          <Toolbar disableGutters>
            <Box style={{ height: "40px", width: "40px", marginRight: "8px" }}>
              <Image src={imgOrange} alt="logo" />
            </Box>
            <Typography variant="h6" noWrap component="div" sx={{ mr: 2, display: { xs: "none", md: "flex" } }} color="primary">
              ITALO
            </Typography>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}
              color="primary">
              ITALO
            </Typography>
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar />
      <Stack direction="row" sx={{ flexGrow: 1, display: "flex" }}>
        <Box
          sx={{
            width: "50vw",
            display: { xs: "none", sd: "none", md: "none", lg: "block" },
            // backgroundImage: "url(https://k8builders.di-k8s.tech.orange/images/k8builders.jpg)",
            backgroundImage: `url(${img.src})`,
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center",
            backgroundSize: "cover",
          }}
        />
        <Stack sx={{ flexGrow: 1 }}>
          <Container sx={{ flexGrow: 1 }}>
            <main>{children}</main>
          </Container>
          <Footer />
        </Stack>
      </Stack>
    </>
  )
}
export default ResponsiveAppBar
