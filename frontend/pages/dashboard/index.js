import React from "react"
import { useQuery } from "react-query"
import Router from "next/router"
import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"

import Appbar from "components/layouts/appbarForm"
import { getProjects } from "api/api.projects"
import { getDatasets } from "api/api.datasets"
import { getUsers } from "api/api.users"
import { getModels } from "api/api.models"
import { getTraces } from "api/api.traces"
import { getStores } from "api/api.stores"

import Dashboard from "components/dashboard"

export default function Index({ query }) {
  const {
    isLoading,
    isError,
    error,
    data: PROJECTS,
    refetch,
  } = useQuery("projects", () => getProjects(0, 0, ["name"]), { keepPreviousData: true })

  const {
    data: DATASETS,
    isFetching: isFetchingDatasets,
    isError: isErrorDatasets,
    error: errorDatasets,
  } = useQuery("allDatasets", () => getDatasets(0, 0), { retry: false })

  const {
    isLoading: isLoadingUsers,
    isError: isErrorUsers,
    error: errorUsers,
    data: USERS,
  } = useQuery("users", () => getUsers(0, 0), { keepPreviousData: true })

  const {
    isLoading: isLoadingModels,
    isError: isErrorModels,
    error: errorModels,
    data: MODELS,
  } = useQuery("models", () => getModels(0, 0), { keepPreviousData: true })

  // const {
  //   isLoading: isLoadingTraces,
  //   isError: isErrorTraces,
  //   error: errorTraces,
  //   data: TRACES,
  // } = useQuery("traces", () => getTraces(0, 0), { keepPreviousData: true })

  const {
    isLoading: isLoadingStores,
    isError: isErrorStores,
    error: errorTStores,
    data: STORES,
  } = useQuery("stores", () => getStores(0, 0), { keepPreviousData: true })

  if (error?.response.status === 401) Router.push("/login")

  return (
    <Appbar>
      <Box sx={{ display: "flex", justifyContent: "space-between" }} pt={2}>
        <Typography variant="h4" color="primary">
          Dashboard
        </Typography>
      </Box>
      <Divider />
      <Dashboard
        projects={PROJECTS?.data}
        datasets={DATASETS?.data}
        users={USERS?.data}
        models={MODELS?.data}
        // traces={TRACES?.data}
        stores={STORES?.data}
        isLoading={isLoading}
        isError={isError}
        refetch={refetch}
      />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

Index.auth = { role: "admin" }
