import csv
from pathlib import Path
from datetime import datetime

from string import digits
import agilkia
import re
import copy

from agilkia import TraceEncoder,TraceSet
import json

remove_digits = str.maketrans('', '', digits)

def process_request_uri(request_uri):
    reg = r"[a-z0-9]*[0-9]+[a-z0-9]*"
    request_uri = re.sub(reg, "", request_uri)

    if "/api/customers//users?limit=&firstName=" in request_uri:
        request_uri = "/api/customers//users?limit=&firstName..."
    if "/api/checklogin?login=" in request_uri:
        request_uri = "/api/checklogin?login=..."
    if "/api/customers//sites//users?login" in request_uri:
        request_uri = "/api/customers//sites//users?login..."
    if "/api/customers//users?limit" in request_uri:
        request_uri = "/api/customers//users?limit..."
    if "/api/sitesGlobalSearch?product=TEAMING&siteClip" in request_uri:
        request_uri = '/api/sitesGlobalSearch?product=TEAMING&siteClip...'
    if "/api/sitesGlobalSearch?product=TEAMING&siteName" in request_uri:
        request_uri = "/api/sitesGlobalSearch?product=TEAMING&siteName..."
    if "/api//customers//sites//packs/" in request_uri:
        request_uri = "/api//customers//sites//packs..."
    if "/api//customers//sites//announcements/" in request_uri:
        request_uri = "/api//customers//sites//announcements/.."
    if 'sendTechnical' in request_uri:
        pass
    return request_uri


def filter_traceset(traceset):
    filtered_traceset = copy.deepcopy(traceset)
    keep_list = []
    for i, tr in enumerate(traceset):
        k = False
        for ev in tr:
            if "api" in ev.action:
                k = True
        if k:
            keep_list.append(tr)

    filtered_traceset.traces = []
    filtered_traceset.traces = keep_list
    return filtered_traceset

def traceset_to_dict(traceset :TraceSet) -> dict:
    str_dict = json.dumps(traceset, cls=TraceEncoder)
    return json.loads(str_dict)


# def read_traces_csv(path: Path) -> agilkia.TraceSet:
def script_teaming(input_file) -> agilkia.TraceSet:
    # with path.open("r") as input:
    trace1 = agilkia.Trace([])
    # for i, line in enumerate(csv.reader(input, delimiter=',')):
    for i,line in enumerate(csv.reader(input_file.read().decode('utf8').splitlines(),delimiter=',')):
        if i == 0:
            sessionID_idx = line.index('session_id')
            request_uri_idx = line.index('request_uri')
            request_method_idx = line.index("request_method")

            time_stamp_idx = line.index('@timestamp')

        timestr = line[0].strip()[0:-4]

        try:

            timestr = datetime.strptime(timestr, "%b %d, %Y @ %H:%M:%S")
            timestamp = timestr.timestamp()
            timestamp = int(timestamp)
            timestamp = datetime.fromtimestamp(timestamp)
            timestamp = timestamp.isoformat('|')

        except:
            print("error")
            continue

        sessionID = line[sessionID_idx].strip()

        request_method = line[request_method_idx].strip()

        request_uri = line[request_uri_idx].strip()

        try:
            status_code = int(status_code)
        except:
            status_code = None

        if 'api' not in request_uri:
            continue
        request_uri = process_request_uri(request_uri)

        action = request_method + '|' + request_uri + '|' + str(status_code)
        inputs = {
            "request_method": request_method,
            "request_uri": request_uri,

        }

        outputs = {}
        others = {
            'timestamp': timestamp,
            'sessionID': sessionID,

        }
        event = agilkia.Event(action, inputs, outputs, others)
        trace1.append(event)

    traceset = agilkia.TraceSet([])
    traceset.append(trace1)
    traceset =traceset.with_traces_grouped_by("sessionID", property=True)
    traceset = filter_traceset(traceset)
    traceset=traceset_to_dict(traceset) 
    return traceset

if __name__=='__main__':
    prod_filenames = ['prod-30j(1).csv']
    test_filenames = ["intg-30j.csv"]
    traceset = read_traces_csv(Path("../data/" + prod_filenames[0]))
    traceset.save_to_json(Path("../data/teaming_v7_2_agilkia_execution_big.json"))
    traceset = read_traces_csv(Path("../data/" + test_filenames[0]))
    traceset.save_to_json(Path("../data/teaming_v7_2_agilkia_test_big.json"))