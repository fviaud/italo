from flask import request, jsonify
from werkzeug.utils import secure_filename
from mongoengine.queryset.visitor import Q
from math import *
import json
from bson import ObjectId
import requests
from model import Stores, Traces
from model import Stores
from convert import script_scanette
from teaming import script_teaming
from pathlib import Path
import os

ALLOWED_EXTENSIONS = {'json', 'csv'}


def api_routes(app):

    def allowed_file(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    @app.route('/api/v0/traces/<pid>/stores', methods=['GET'])
    def stores(pid):
        userId = request.headers.get("X-Forwarded-User")
        page = request.args.get('page', 1, type=int)
        limit = request.args.get('limit', 10, type=int)
        totalPages = 0 if limit <= 0 else ceil(
            Stores.objects(project=pid).count()/limit)
        # stores = Stores.objects(Q(public=True) | Q(
        #     author=userId)).skip((page-1)*limit).limit(limit)
        fields = request.args.getlist('fields[]', type=str)
        if len(fields) > 0 and '_id' not in fields:
            stores = Stores.objects(project=pid).only(
                *fields).skip((page-1)*limit).limit(limit)
        else:
            stores = Stores.objects(project=pid).skip(
                (page-1)*limit).limit(limit)
        return jsonify(
            {
                "page": page,
                "results": list(stores),
                "totalPages": totalPages,
                "documents": Stores.objects().count()
            }
        ), 200

    @app.route('/api/v0/traces/file', methods=['POST'])
    def upload_file():
        try:
            file = request.files["trace"]
            if file.filename == '':
                return jsonify({'message': 'No selected file'})
            if not file or not allowed_file(file.filename):
                return jsonify({'message': 'Not file or error file format'})

            filename = secure_filename(file.filename)
            trace = Traces()
            trace.name = request.form['name']
            trace.description = request.form['description']
            trace.filename = filename
            trace.project = request.form['project']
            trace.author = request.form['author']
            trace.file.put(file, content_type='application/json')
            trace.save()

            if request.form['type_trace'] == "Scanette":
                traceset = script_scanette(trace.file)
            if request.form['type_trace'] == "Teaming":
                traceset = script_teaming(trace.file)
            if request.form['type_trace'] == "Json file":
                traceset = json.loads(trace.file.read())

            store = Stores()
            store.name = request.form['name']
            store.description = request.form['description']
            store.project = request.form['project']
            store.author = request.form['author']
            store.file.new_file()
            store.file.write(json.dumps(traceset).encode('utf8'))
            store.file.close()
            store.save()

            trace.delete()

            return jsonify({'save file': 'done'}), 200
        except:
            return jsonify({'save file': 'failed'}), 400

    # @app.route('/api/v0/traces', methods=['PATCH'])
    # def update_one_traces():
    #     try:
    #         userId = request.headers.get("X-Forwarded-User")
    #         dataset = Traces.objects(Q(pk=request.form['id']) & (Q(public=True) | Q(author=userId))).update(
    #             set__filename=request.form['filename'],
    #             set__public=json.loads(request.form['public']),
    #             # set__description=request.form['description'],

    #         )
    #         return jsonify({"update": "ok"}), 200
    #     except KeyError:
    #         return 'Format not bvalid in the request body', 400

    @app.route('/api/v0/traces/stores/<id>', methods=['DELETE'])
    def delete_one_store(id):
        try:
            userId = request.headers.get("X-Forwarded-User")
            store = Stores.objects(pk=id)
            store[0].delete()
            return jsonify(store.to_json()), 200
        except:
            return jsonify({'Delette trace': 'failed'}), 400

    @app.route('/api/v0/traces/job/<id>', methods=['GET'])
    def traceByCluster(id):
        try:
            idmodel = request.args.get('idmodel', 0, type=int)
            idcluster = request.args.get('idcluster', 0)
            page = request.args.get('page', 1, type=int)
            limit = request.args.get('limit', 10, type=int)
            job = requests.get(
                "http://projects/api/v0/projects/jobs/"+id).json()[0]

            data_json = {}
            for item in job["execution_traces"]:
                store = Stores.objects(pk=ObjectId(item))
                if not data_json:
                    data_json = json.loads(store[0].file.read())
                else:
                    data_json["traces"].extend(
                        json.loads(store[0].file.read())["traces"])

            def cluster(item):
                return int(item) == int(idcluster)

            # if limit > 0:
            #     totalPages = ceil(len(list(filter(cluster, list(
            #         job["models"][idmodel]["results"]["execution_clusters"]))))/limit)
            # else:
            #     totalPages = 0

            data = []
            count = 0
            seen = dict()

            for item in enumerate(list(job["models"][idmodel]["results"]["execution_clusters"])):
                if int(item[1]) == int(idcluster):
                    key = ""
                    for event in data_json["traces"][item[0]]["events"]:
                        key += event["action"]
                    if key not in seen:
                        if (count >= (page-1)*limit) & (count < (page-1)*limit+limit):
                            data.append(data_json["traces"][item[0]]["events"])
                        count = count+1
                        seen[key] = 1
                    else:
                        seen[key] += 1

            totalPages = ceil(count / limit)
            results = []

            for trace in data:
                key = ""
                for event in trace:
                    key += event["action"]
                results.append({"events": trace, "counter": seen[key]})

            return {
                "project_name": job["name"],
                "model_name": job["models"][idmodel]["name"],
                "results": results,
                "totalPages": totalPages,
                "page": page
            }
        except:
            return "error", 400

    @app.route('/api/v0/traces/job/<id>/tests', methods=['GET'])
    def testTraceByCluster(id):
        try:
            idmodel = request.args.get('idmodel', 0, type=int)
            idcluster = request.args.get('idcluster', 0)
            page = request.args.get('page', 1, type=int)
            limit = request.args.get('limit', 10, type=int)
            job = requests.get(
                "http://projects/api/v0/projects/jobs/"+id).json()[0]

            data_json = {}
            for item in job["test_traces"]:
                store = Stores.objects(pk=ObjectId(item))
                if not data_json:
                    data_json = json.loads(store[0].file.read())
                else:
                    data_json["traces"].extend(
                        json.loads(store[0].file.read())["traces"])

            def cluster(item):
                return int(item) == int(idcluster)

            # if limit > 0:
            #     totalPages = ceil(len(list(filter(cluster, list(
            #         job["models"][idmodel]["results"]["execution_clusters"]))))/limit)
            # else:
            #     totalPages = 0

            data = []
            count = 0
            seen = dict()

            for item in enumerate(list(job["models"][idmodel]["results"]["test_clusters"])):
                if int(item[1]) == int(idcluster):
                    key = ""
                    for event in data_json["traces"][item[0]]["events"]:
                        key += event["action"]
                    if key not in seen:
                        if (count >= (page-1)*limit) & (count < (page-1)*limit+limit):
                            data.append(data_json["traces"][item[0]]["events"])
                        count = count+1
                        seen[key] = 1
                    else:
                        seen[key] += 1

            totalPages = ceil(count / limit)
            results = []

            for trace in data:
                key = ""
                for event in trace:
                    key += event["action"]
                results.append({"events": trace, "counter": seen[key]})

            return {
                "project_name": job["name"],
                "model_name": job["models"][idmodel]["name"],
                "results": results,
                "totalPages": totalPages,
                "page": page
            }
        except:
            return "error", 400
