import React from 'react'
import { useQuery } from "react-query"
import Link from 'next/link'
import Router from "next/router"

import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import Divider from '@mui/material/Divider'
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline'
import Button from '@mui/material/Button'
import Users from "components/users/items"
import Appbar from "components/layouts/appbar"
import { getUsers } from 'api/api.users'

export default function Index({ query }) {
    const page = query.page
    const fetchObjects = (page = 1) => getUsers(page)
    const { isLoading, isError, error, data: USERS, refetch, } = useQuery(['users', page], () => fetchObjects(page), { keepPreviousData: true })

    if (error?.response.status === 401) Router.push("/login")

    return (
        <Appbar>
            <Box sx={{ display: 'flex', justifyContent: 'space-between' }} mt={2}>
                <Typography variant="h4" color="primary" >Users</Typography>
                <Link href={"/users/new/"} style><Button disabled variant="text" startIcon={<AddCircleOutlineIcon />}>Add</Button></Link>
            </Box>
            <Divider />
            <Users USERS={USERS} page={page} isLoading={isLoading} isError={isError} refetch={refetch} />
        </Appbar>)
}
export function getServerSideProps(context) {
    return { props: { query: context.query } }
}

Index.auth = { role: "admin" }