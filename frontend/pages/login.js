import React from "react"
import Link from "next/link"

import Appbar from "components/layouts/appbar_min"
import Button from "@mui/material/Button"
import Container from "@mui/material/Container"
import Stack from "@mui/material/Stack"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"
import VideoPresentation from "components/videoPresentation/VideoPresentation"

function Index() {
  return (
    <Appbar>
      <Container>
        <Stack spacing={2} mt={5}>
          <Typography variant="h4" color="primary">
            The IA tool you always wanted
          </Typography>
          <Box sx={{ width: 1 / 2 }}>
            <Stack spacing={2} mt={5}>
              {/* <Link href={`${process.env.NEXT_PUBLIC_API_AUTH}/login/orange`}>
                <Button variant="contained">Getting started with Orange Connect</Button>
              </Link> */}
              <Link href={`${process.env.NEXT_PUBLIC_API_AUTH}/login/google`} >
                <Button variant="contained" sx={{
                  bgcolor: '#4c8bf5', color: "white",
                  '&:hover': {
                    backgroundColor: '#4c8bf5',
                    boxShadow: 'none',
                  }
                }} >Getting started with Google</Button>
              </Link>
              {/* <Link href={`${process.env.NEXT_PUBLIC_API_AUTH}/login/facebook`} >
                <Button variant="contained" sx={{
                  bgcolor: '#3b5998', color: "white",
                  '&:hover': {
                    backgroundColor: '#3b5998',
                    boxShadow: 'none',
                  }
                }} >Getting started with facebook</Button>
              </Link> */}
            </Stack>

          </Box>
        </Stack>
      </Container>
    </Appbar>
  )
}

export default Index
