werkzeug==2.1.2
Flask==2.1.2
Authlib==1.0.1
requests==2.27.1
pymongo==4.0.1
flask-login==0.6.1
flask-mongoengine==1.0.0
flask_bcrypt==1.0.1