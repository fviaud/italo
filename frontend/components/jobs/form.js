import React from "react"
import { useQueryClient } from "react-query"
import Router from "next/router"

import { useFormikContext, Formik, Form, Field, ErrorMessage } from "formik"
import * as Yup from "yup"

import Box from "@mui/material/Box"
import TextField from "@mui/material/TextField"
import MenuItem from "@mui/material/MenuItem"
import FormControlLabel from "@mui/material/FormControlLabel"
import Checkbox from "@mui/material/Checkbox"
import { FormGroup } from "@mui/material"
import { FormLabel } from "@mui/material"
import Stack from "@mui/material/Stack"
import { LoadingButton } from "@mui/lab"
import FormHelperText from "@mui/material/FormHelperText"
import HourglassEmptyIcon from "@mui/icons-material/HourglassEmpty"
import Typography from "@mui/material/Typography"
import FormControl from "@mui/material/FormControl"
import InputLabel from "@mui/material/InputLabel"
import Paper from "@mui/material/Paper"
import Select from "@mui/material/Select"
import Button from "@mui/material/Button"

import HelpTooltip from "components/helpTooltip"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"

import { addJobs, updateJobs } from "api/api.jobs"

const GENERATION = {
  data: {
    results: [
      { name: "Decision Trees" },
      { name: "multilayerperceptron" },
      { name: "Logistic Regression" },
      { name: "Transformers" },
    ],
  },
}

function CheckboxGroup(props) {
  const { label, name, options, formik, helpText, additionalInput, ...rest } = props
  return (
    <Paper variant="outlined">
      <Box sx={{ p: 2, paddingTop: 1, display: "flex" }}>
        <FormLabel>
          <Typography variant="h6" style={{ fontWeight: 600 }} color="primary">
            {label}
          </Typography>
        </FormLabel>
        <HelpTooltip label={helpText} placement={"right"} color="primary" />
      </Box>
      <FormGroup>
        <Field name={name}>
          {({ field }) => {
            return options?.data?.results.map((option) => {
              return (
                <React.Fragment key={option.name}>
                  <Box sx={{ p: 2, mt: -4 }}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          id={option.value}
                          {...field}
                          {...rest}
                          // value={option.id}
                          // checked={field.value.includes(option.id)}
                          value={option.name}
                          checked={field.value.includes(option.name)}
                          disabled={
                            ![
                              "kmeans",
                              "kmeans30",
                              "kmeans100",
                              "meanshift",
                              "dbscan",
                              "spectralclustering",
                              "Decision Trees",
                              "multilayerperceptron",
                            ].includes(option.name)
                          }
                        />
                      }
                      label={option.name}
                    />
                  </Box>
                </React.Fragment>
              )
            })
          }}
        </Field>
      </FormGroup>
      <ErrorMessage component={TextError} name={name} />
      {additionalInput}
    </Paper>
  )
}

function TextError(props) {
  return <FormHelperText error>{props.children}</FormHelperText>
}

function Estimate() {
  const { values } = useFormikContext()
  const estimate = values.models.length * 2 + values.generativeModels.length * 1
  return (
    <Button startIcon={<HourglassEmptyIcon />} variant="outlined">
      {`Estimate ${estimate} min`}
    </Button>
  )
}

function FormProject({ job, isLoading, isError, STORES, MODELS, pid }) {
  const queryClient = useQueryClient()
  const curentSession = queryClient.getQueryData("session")

  if (isLoading) return <Spinner />
  if (isError) return <Error />

  const initialValues = {
    name: job?.name ?? "",
    description: job?.description ?? "",
    execution_traces: job?.execution_traces ?? [],
    test_traces: job?.test_traces ?? [],
    models: job?.models
      ? job.models.reduce((acc, curr) => {
          acc.push(curr.name)
          return acc
        }, [])
      : [],
    generativeModels: job?.generativeModels
      ? job.generativeModels.reduce((acc, curr) => {
          acc.push(curr.name)
          return acc
        }, [])
      : [],
    nbScenarios: job?.nbScenarios ?? 0,
    public: job?.public ?? true,
  }
  const validationSchema = Yup.object({
    name: Yup.string().required("Is required"),
    // description: Yup.string().required("Is required"),
    execution_traces: Yup.array().min(1).required("Is required"),
    test_traces: Yup.array().min(1).required("Is required"),
    models: Yup.array().min(1, "At least one model is required"),
    // generativeModels: Yup.array().min(1, "At least one model is required")
  })

  const onSubmit = async (values) => {
    const formData = new FormData()
    if (job) {
      formData.append("id", job._id.$oid)
    }
    formData.append("author", curentSession.data._user_id)
    formData.append("name", values.name)
    formData.append("description", values.description)
    formData.append("project", pid)
    formData.append("execution_traces", JSON.stringify(values.execution_traces))
    formData.append("test_traces", JSON.stringify(values.test_traces))
    formData.append("models", JSON.stringify(MODELS.data.results.filter((model) => values.models.includes(model.name))))
    formData.append(
      "generativeModels",
      JSON.stringify(GENERATION.data.results.filter((model) => values.generativeModels.includes(model.name)))
    )
    formData.append("nbScenarios", values.nbScenarios || 0)
    formData.append("status", "in progress")

    try {
      ;(await job?.name) ? updateJobs(formData) : addJobs(formData)
      notifMessage("success", `${job?.name ? "Update " : "Add "}job with success !`)
    } catch (error) {
      notifMessage("error", "Failed add or update job!")
    }
    Router.back()
  }

  return (
    <Stack mt={2}>
      {/* <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h6" color="primary">
          {job ? "Modify job" : "New job"}
        </Typography>
      </Box> */}
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {(formik) => (
          <Form autoComplete="off" noValidate>
            <Stack spacing={2} sx={{ pt: 2 }}>
              <Paper variant="outlined">
                <Stack>
                  <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                    <FormLabel>
                      <Typography variant="h6" style={{ fontWeight: 600 }} color={"primary"}>
                        Naming
                      </Typography>
                    </FormLabel>
                  </Box>
                  <Box sx={{ p: 2 }}>
                    <TextField
                      id="name"
                      name="name"
                      label="Name"
                      fullWidth
                      required
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.name && formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                  </Box>
                  <Box sx={{ p: 2 }}>
                    <TextField
                      id="description"
                      name="description"
                      label="Description (optionnal)"
                      fullWidth
                      multiline
                      maxRows={4}
                      // required
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.description && formik.errors.description)}
                      helperText={formik.touched.description && formik.errors.description}
                    />
                  </Box>
                  {/* {DATASETS?.data?.results && (
                    <Box sx={{ p: 2, display: "flex", alignItems: "center" }}>
                      <TextField
                        id="dataset"
                        name="dataset"
                        label="Dataset"
                        select
                        required
                        value={formik.values.dataset ?? ""}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        fullWidth
                        error={Boolean(formik.touched.dataset && formik.errors.dataset)}
                        helperText={formik.touched.dataset && formik.errors.dataset}>
                        {DATASETS?.data.results.map((value, index) => (
                          <MenuItem key={index} value={value}>
                            {value.name}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Box>
                  )} */}
                </Stack>
              </Paper>
              <Paper variant="outlined">
                <Stack>
                  <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                    <FormLabel>
                      <Typography variant="h6" style={{ fontWeight: 600 }} color={"primary"}>
                        Dataset
                      </Typography>
                    </FormLabel>
                  </Box>
                  <Box sx={{ p: 2, paddingTop: 0 }}>
                    <Stack spacing={2} sx={{ mt: 2 }}>
                      {STORES?.data?.results && (
                        <>
                          <Box sx={{ display: "flex" }}>
                            <TextField
                              id="execution_traces"
                              name="execution_traces"
                              label="execution_traces"
                              select
                              required
                              value={formik.values.execution_traces}
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              fullWidth
                              error={Boolean(formik.touched.execution_traces && formik.errors.execution_traces)}
                              helperText={formik.touched.execution_traces && formik.errors.execution_traces}
                              SelectProps={{
                                multiple: true,
                                renderValue: (selected) => {
                                  const tracesSelected = STORES?.data.results.filter((t) => selected.indexOf(t._id.$oid) > -1)
                                  return tracesSelected.map((t) => t.name).join(",")
                                },
                              }}>
                              {STORES?.data.results.map((value, index) => (
                                <MenuItem value={value._id.$oid} key={index}>
                                  <Checkbox checked={formik.values.execution_traces.indexOf(value._id.$oid) > -1} />
                                  {value.name}
                                </MenuItem>
                              ))}
                            </TextField>
                            <HelpTooltip label={"help text 5"} />
                          </Box>
                          <Box sx={{ display: "flex" }}>
                            <TextField
                              id="test_traces"
                              name="test_traces"
                              label="test_traces"
                              select
                              SelectProps={{
                                multiple: true,
                                renderValue: (selected) => {
                                  const tracesSelected = STORES?.data.results.filter((t) => selected.indexOf(t._id.$oid) > -1)
                                  console.log(tracesSelected)
                                  return tracesSelected.map((t) => t.name).join(",")
                                },
                              }}
                              required
                              value={formik.values.test_traces}
                              onChange={formik.handleChange}
                              onBlur={formik.handleBlur}
                              fullWidth
                              error={Boolean(formik.touched.test_traces && formik.errors.test_traces)}
                              helperText={formik.touched.test_traces && formik.errors.test_traces}>
                              {STORES?.data.results.map((value, index) => (
                                <MenuItem value={value._id.$oid} key={index}>
                                  <Checkbox checked={formik.values.test_traces.indexOf(value._id.$oid) > -1} />
                                  {value.name}
                                </MenuItem>
                              ))}
                            </TextField>
                            <HelpTooltip label={"help text 6"} />
                          </Box>
                        </>
                      )}
                    </Stack>
                  </Box>
                </Stack>
              </Paper>

              {MODELS?.data?.results && (
                <CheckboxGroup
                  control="checkbox"
                  label="Clustering"
                  name="models"
                  options={MODELS}
                  helpText={"help text 1"}
                  formik={formik}
                />
              )}
              <CheckboxGroup
                control="checkbox"
                label="Generation (Optionnel)"
                name="generativeModels"
                options={GENERATION}
                helpText={"help text 2"}
                formik={formik}
                additionalInput={
                  <Box sx={{ p: 2, display: "flex" }}>
                    <FormControl fullWidth>
                      <InputLabel id="demo-simple-select-label">Number of generated scenarios per cluster​</InputLabel>
                      <Select
                        id="nbScenarios"
                        name="nbScenarios"
                        value={formik.values.nbScenarios || 1}
                        label="Total Number of Test Scenarios​"
                        onChange={formik.handleChange}>
                        <MenuItem value={1}>1</MenuItem>
                        <MenuItem value={2}>2</MenuItem>
                        <MenuItem value={3}>3</MenuItem>
                        <MenuItem value={4}>4</MenuItem>
                        <MenuItem value={5}>5</MenuItem>
                        <MenuItem value={6}>6</MenuItem>
                        <MenuItem value={7}>7</MenuItem>
                        <MenuItem value={8}>8</MenuItem>
                        <MenuItem value={9}>9</MenuItem>
                        <MenuItem value={10}>10</MenuItem>
                        <MenuItem value={20}>20</MenuItem>
                        <MenuItem value={30}>30</MenuItem>
                        <MenuItem value={40}>40</MenuItem>
                      </Select>
                    </FormControl>
                    <HelpTooltip label={"help text 3"} placement={"right"} />
                  </Box>
                }
              />
              <Stack spacing={2} sx={{ pt: 2 }} direction="row">
                <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                  {job ? "update" : "Submit"}
                </LoadingButton>
                <Button color="primary" onClick={() => Router.back()}>
                  Cancel
                </Button>
              </Stack>
            </Stack>
          </Form>
        )}
      </Formik>
    </Stack>
  )
}

export default FormProject
