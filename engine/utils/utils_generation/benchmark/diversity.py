from collections import Counter
from agilkia import TraceSet
from . ngramcoverage import extract_all_n_grams
def relative_diversity(traceset, n=1) -> float:
    """Calculates the 'distinct-n' metric for all the traces in the given set.
    For example, n=2 means count the number of distinct adjacent pairs of
    action names in the traces, and divide that by the total number of pairs.
    The result will always be between 0.0 to 1.0.
    These metrics are useful as a measure of diversity.
    """
    if type(traceset)==TraceSet:
        counts = Counter()
        for tr in traceset:
            for i in range(0, len(tr) - n):
                actions = ",".join([ev.action for ev in tr[i:i+n]])
                counts[actions] += 1
        return len(counts) / sum(counts.values())

    elif type(traceset)==list:
        counts = Counter()
        for tr in traceset:
            for i in range(0, len(tr) - n):
                actions = ",".join([ev for ev in tr[i:i + n]])
                counts[actions] += 1
        return len(counts) / sum(counts.values())
    else:
        raise ValueError('wrong traceset type, must be list of list or agilkia traceset')

def absolute_diversity(candidate_traceset,reference_traceset,n=1):
    if type(candidate_traceset)==TraceSet:
        counts = Counter()
        for tr in candidate_traceset:
            for i in range(0, len(tr) - n):
                actions = ",".join([ev.action for ev in tr[i:i+n]])
                counts[actions] += 1
        return len(counts) / len(extract_all_n_grams(reference_traceset,n=n))

    elif type(candidate_traceset)==list:
        counts = Counter()
        for tr in candidate_traceset:
            for i in range(0, len(tr) - n):
                actions = ",".join([ev for ev in tr[i:i + n]])
                counts[actions] += 1
        return len(counts) / len(extract_all_n_grams(reference_traceset,n=n))
    else:
        raise ValueError('wrong traceset type, must be list of list or agilkia traceset')