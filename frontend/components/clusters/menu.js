import * as React from "react"
import Box from "@mui/material/Box"
import Tab from "@mui/material/Tab"
import TabContext from "@mui/lab/TabContext"
import TabList from "@mui/lab/TabList"
import TabPanel from "@mui/lab/TabPanel"
import Chip from "@mui/material/Chip"

import Details from "./details"
import TestDetails from "./testdetails"
import Synthese from "./synthese"

export default function LabTabs({ CLUSTER, TESTCLUSTER, idcluster, menu = "synthese", query, page, testPage, job, pid, jid }) {
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  const [value, setValue] = React.useState(menu)
  const pages = [
    {
      name: "synthese",
      label: "Synthèse",
      component: <Synthese job={job} query={query} idcluster={idcluster} />,
    },
    {
      name: `execution`,
      label:(<div>
        Execution traces
        <Chip size="small" label={Number.parseInt(CLUSTER.data.results.map(trace => trace.counter).reduce((p,c) => p+c,0))} />
      </div>),
      component: <Details CLUSTER={CLUSTER} query={query} page={page} menu={value} pid={pid} jid={jid} />,
    },
    {
      name: `test`,
      label:(<div>
        Test traces
        <Chip size="small" label={Number.parseInt(TESTCLUSTER.data.results.map(trace => trace.counter).reduce((p,c) => p+c,0))} />
      </div>),
      component: <TestDetails CLUSTER={TESTCLUSTER} query={query} page={testPage} menu={value} pid={pid} jid={jid} />,
    },
  ]

  return (
    <Box sx={{ width: "100%", typography: "body1" }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList onChange={handleChange}>
            {pages.map((item, index) => (
              <Tab style={{ textTransform: "none" }} key={index} label={item.label} value={item.name} />
            ))}
          </TabList>
        </Box>
        {pages.map((item, index) => (
          <TabPanel value={item.name} key={index}>
            {item.component}
          </TabPanel>
        ))}
      </TabContext>
    </Box>
  )
}
