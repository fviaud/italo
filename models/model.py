from flask_mongoengine import MongoEngine
db = MongoEngine()

class Models(db.Document):
    name= db.StringField(max_length=200)
    description= db.StringField(max_length=200)
    author= db.StringField(max_length=200)

    def to_json(self):
        return {"name": self.name}




