import React from "react"
import { useQuery } from "react-query"
import Router from "next/router"

import Form from "components/jobs/form"
import Appbar from "components/layouts/appbarForm"

import { getStores } from "api/api.stores"
import { getModels } from "api/api.models"
import { getJob } from "api/api.jobs"
export default function Index({ params }) {
  const { pid, jid } = params

  const {
    isFetching: isFetchingJob,
    isError: isErrorJob,
    data: JOB,
    error: errorJob,
  } = useQuery("job", () => getJob(jid), { retry: false })

  const {
    data: STORES,
    isLoading: isLoadingStores,
    isError: isErrorStores,
    error: errorStores,
  } = useQuery("allStores", () => getStores(0, 0, pid, ["name", "description", "project"]), { retry: false })

  const {
    data: MODELS,
    isLoading: isLoadingModels,
    isError: isErrorModels,
    error: errorModels,
  } = useQuery("allModels", () => getModels(0, 0), { retry: false })

  if (errorStores?.response.status === 401 || errorModels?.response.status === 401 || errorJob?.response.status === 401)
    Router.push("/login")

  const isLoading = isLoadingStores || isLoadingModels || isFetchingJob
  const isError = isErrorStores || isErrorModels || isErrorJob

  return (
    <Appbar>
      <Form job={JOB?.data[0]} STORES={STORES} MODELS={MODELS} isLoading={isFetchingJob} isError={isError} pid={pid} />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params } }
}

Index.auth = { role: "admin" }
