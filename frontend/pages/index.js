import React, { useEffect } from "react"
import Router from "next/router"
import Appbar from "components/layouts/appbar"
import Spinner from "components/utils/spinner"

export default function Index() {
  useEffect(() => {
    Router.push("/dashboard")
  }, [])

  return (
    <Appbar>
      <Spinner />
    </Appbar>
  )
}
Index.auth = { role: "admin" }
