import React from "react"
import Link from "next/link"
import Router, { useRouter } from "next/router"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import IconButton from "@mui/material/IconButton"
import DeleteIcon from "@mui/icons-material/Delete"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Pagination from "@mui/material/Pagination"
import FileDownloadIcon from "@mui/icons-material/FileDownload"
import TaskIcon from "@mui/icons-material/Task"
import FileUploadIcon from "@mui/icons-material/FileUpload"
import ArticleIcon from "@mui/icons-material/Article"
import Typography from "@mui/material/Typography"

import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"
import { delTraces, convertTraces } from "api/api.traces"
import { stringToHslColor } from "helpers"

export default function Items({ TRACES, isLoading, page = 0, refetch, isError, upLoading }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />
  return (
    <Stack mt={2}>
      {TRACES.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {TRACES.data.results.map((trace, index) => (
          <ListItemButton dense disableRipple key={index} onClick={() => Router.push(`/traces/${trace._id.$oid}`)}>
            <ListItem
              secondaryAction={
                <>
                  <IconButton
                    edge="end"
                    onClick={async (e) => {
                      e.stopPropagation()
                      if (window.confirm("Do you wand delete this object ?")) {
                        try {
                          await delTraces(trace._id.$oid)
                          refetch()
                          notifMessage("success", "Delete trace with success !")
                        } catch (error) {
                          notifMessage("error", "Failed to delete trace !")
                          refetch()
                        }
                      }
                    }}>
                    <DeleteIcon />
                  </IconButton>
                  {/* <IconButton
                    edge="end"
                    onClick={async (e) => {
                      e.stopPropagation()
                      if (window.confirm("Do you wand convert this object ?")) {
                        try {
                          await convertTraces(trace._id.$oid)
                          refetch()
                          notifMessage("success", "Convert trace with success !")
                        } catch (error) {
                          notifMessage("error", "Failed to convert trace !")
                          refetch()
                        }
                      }
                    }}>
                    <TaskIcon />
                  </IconButton> */}
                </>
              }>
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: stringToHslColor(trace.filename) }}>
                  <ArticleIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={<>{trace.filename}</>} />
            </ListItem>
          </ListItemButton>
        ))}
        {upLoading && (
          <ListItemButton dense disableRipple>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <FileUploadIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={<Typography color="primary">Uploading file in progress...</Typography>} />
            </ListItem>
          </ListItemButton>
        )}
      </List>
      {(TRACES.data.totalPages > 1 || TRACES.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={TRACES.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/traces/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
