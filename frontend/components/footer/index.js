import React from 'react';
import { Typography, Box, Link } from "@mui/material"

const version = "0.1.0"

const Footer = () => {
    return (
        <>
            <Box sx={{ 
                p: 1,
                paddingTop: 4,
                margin: "auto",
            }}>
                <Typography variant="body1" align="right">
                    Powered by{' '}
                    <Link
                        component="a"
                        href="https://github.com/PHILAE-PROJECT/agilkia"
                        target="_blank"
                        sx={{color:"#000", textDecoration:"none"}}
                    >
                        PHILAE
                    </Link>
                    {' - '}&copy;{' '}
                    <Link
                        component="a"
                        href="https://www.orange.fr/"
                        target="_blank"
                        sx={{color:"#000", textDecoration:"none"}}
                    >
                        Orange
                    </Link>
                    . 2022 - v{version}-beta
                </Typography>
            </Box>
        </>
    );
};

export default Footer;