import React from "react"
import Box from "@mui/material/Box"
import CircularProgress from "@mui/material/CircularProgress"

export default function CircularIndeterminate() {
  return (
    <Box style={{ transform: "translate(-50%, -50%)" }} position="absolute" top="50%" left="50%" textAlign="center">
      <CircularProgress />
    </Box>
  )
}
