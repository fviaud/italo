import React from "react"

import Container from "@mui/material/Container"
import Stack from "@mui/material/Stack"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"
import Image from "next/image"

import about from "public/about.jpg"

function About() {
  return (
    <Container>
      <Stack spacing={2} mt={5}>
        <Typography sx={{ marginBottom: 2 }}>
          Italo is currently maintained by the INNOV/IT-S/IVA/DOPSI/DOC team. The web application is built over the Philae Toolbox
          and its logs processing library Agilkia
        </Typography>
        <Typography>
          <a href="https://github.com/philae-project">https://github.com/philae-project</a>
        </Typography>
        <Typography sx={{ marginBottom: 2 }}>
          <a href="https://github.com/PHILAE-PROJECT/agilkia">https://github.com/PHILAE-PROJECT/agilkia</a>
        </Typography>
        <Typography sx={{ marginBottom: 2 }}>The web-application architecture is shown below :</Typography>
        <Box sx={{ position: "relative", height: "40vh", width: "35vw", margin: "auto" }}>
          <Image src={about} alt="about-schematic" layout="fill" />
        </Box>
      </Stack>
    </Container>
  )
}

export default About
