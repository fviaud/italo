import React from "react"
import Box from "@mui/material/Box"
import Router from "next/router"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"

export default function SimpleReturnPage({ message }) {
  return (
    <Box style={{ transform: "translate(-50%, -50%)" }} position="absolute" top="50%" left="50%" textAlign="center">
      <Typography variant="h4" component="div" gutterBottom>
        {message}
      </Typography>
      {/* <Button variant="outlined" startIcon={<ArrowBackIcon />} onClick={() => Router.back()}>
        Go back
      </Button> */}
    </Box>
  )
}
