export const stringToHslColor = (str = "empty", saturation = 50, ligthness = 70) => {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }

    var h = (hash + str.length) % 360;
    return 'hsl(' + h + ', ' + saturation + '%, ' + ligthness + '%)';
}