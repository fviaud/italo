import * as React from "react"
import Router, { useRouter } from "next/router"
import Link from "next/link"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import IconButton from "@mui/material/IconButton"
import DeleteIcon from "@mui/icons-material/Delete"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Pagination from "@mui/material/Pagination"
import FileCopyIcon from "@mui/icons-material/FileCopy"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"

import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"

import { delDatasets } from "api/api.datasets"
import { stringToHslColor } from "helpers"

export default function Items({ DATASETS, isLoading, page = 0, refetch, isError }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />
  return (
    <Stack mt={2}>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h4" color="primary"></Typography>
        <Link href={"/datasets/new/"} style>
          <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
            Add
          </Button>
        </Link>
      </Box>
      {DATASETS.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {DATASETS.data.results.map((dataset, index) => (
          <ListItemButton dense disableRipple key={index} onClick={() => Router.push(`/datasets/edit/${dataset._id.$oid}`)}>
            <ListItem
              secondaryAction={
                <IconButton
                  edge="end"
                  onClick={async (e) => {
                    e.stopPropagation()
                    if (window.confirm("Do you wand delete this object ?")) {
                      try {
                        await delDatasets(dataset._id.$oid)
                        refetch()
                        notifMessage("success", "Delete dataset with success !")
                      } catch (error) {
                        notifMessage("error", "Failed to delete dataset !")
                      }
                    }
                  }}>
                  <DeleteIcon />
                </IconButton>
              }>
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: stringToHslColor(dataset.name) }}>
                  <FileCopyIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={dataset.name}
                // secondary={
                //     <span>
                //         {`Execution_traces: ${dataset.execution_traces.filename}`} <br />
                //         {`Test_traces: ${dataset.test_traces.filename}`}
                //     </span>
                // }
              />
            </ListItem>
          </ListItemButton>
        ))}
      </List>
      {(DATASETS.data.totalPages > 1 || DATASETS.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={DATASETS.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/datasets/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
