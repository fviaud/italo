import React from "react"
import { useQuery } from "react-query"
import Link from "next/link"
import Router from "next/router"

import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import Button from "@mui/material/Button"
import Projects from "components/projects/items"
import Appbar from "components/layouts/appbar"
import { getProjects } from "api/api.projects"

export default function Index({ query }) {
  const page = query.page
  const fetchObjects = (page = 1) => getProjects(page, 10, ["name", "public", "status"])
  const {
    isLoading,
    isError,
    error,
    data: PROJECTS,
    refetch,
  } = useQuery(["projects", page], () => fetchObjects(page), { keepPreviousData: true })

  if (error?.response.status === 401) Router.push("/login")

  return (
    <Appbar>
      <Box sx={{ display: "flex", justifyContent: "space-between" }} mt={2}>
        <Typography variant="h4" color="primary">
          Projects
        </Typography>
        <Link href={"/projects/new/"} style>
          <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
            Add
          </Button>
        </Link>
      </Box>
      <Divider />
      <Projects PROJECTS={PROJECTS} page={page} isLoading={isLoading} isError={isError} refetch={refetch} />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

Index.auth = { role: "admin" }
