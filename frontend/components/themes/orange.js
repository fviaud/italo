import { createTheme } from "@mui/material/styles"
import { red } from "@mui/material/colors"

// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: "#FF7900",
    },
    secondary: {
      main: "#FF7900",
    },
    error: {
      main: red.A400,
    },
  },
  components: {
    MuiAppBar: {
      styleOverrides: {
        colorPrimary: {
          backgroundColor: "black",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        colorPrimary: {
          backgroundColor: "black",
        },
      },
    },
  },
})

export default theme
