import React from "react"
import Router from "next/router"

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale, //Pour l'instant ne pas supprimer
  LogarithmicScale, //Pour l'instant ne pas supprimer
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js"
import { Bar } from "react-chartjs-2"

const getTracesDistribution = (labels, traces_distribution, untested = []) => {
  if (untested.length > 0) return labels.map((value, index) => {
    if (untested.indexOf(index) >= 0) return traces_distribution[index][1];
    else return 0;
  });
  else return labels.map((value, index) => traces_distribution[index][1])
}

export default function App({ job, model, indexModel, unTested = false }) {
  ChartJS.register(CategoryScale, LinearScale, LogarithmicScale, BarElement, Title, Tooltip, Legend)

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: model.name,
        // text: Math.round(model.results.score * 100),
      },
    },
    scales: {
      yAxes: {
        type: "logarithmic",
      },
    },
    onClick: function (evt, element) {
      if (element && element[0])
        // Router.push(`/projects/traces?idjob=${job._id.$oid}&idmodel=${indexModel}&idcluster=${element[0].index}`)
        Router.push(`/projects/${job.project}/jobs/${job._id.$oid}/details?idmodel=${indexModel}&idcluster=${element[0].index}`)
    },
    onHover: (event, chartElement) => {
      const target = event.native ? event.native.target : event.target
      target.style.cursor = chartElement[0] ? "pointer" : "default"
    },
  }

  const labels = model.results["execution_traces_distribution"].map((value, index) => index)
  const data = {
    labels,
    datasets: [
      {
        label: "execution_traces_distribution",
        data: labels.map((value, index) => {
          if (Array.isArray(unTested)) return unTested.indexOf(index) < 0 ? 0 : model.results["execution_traces_distribution"][index][1];
          return model.results["execution_traces_distribution"][index][1];
        }),
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
      {
        label: "test_traces_distribution",
        data: labels.map((value, index) => {
          if (Array.isArray(unTested)) return unTested.indexOf(index) < 0 ? 0 : model.results["test_traces_distribution"][index] ? model.results["test_traces_distribution"][index][1] : 0;
          return model.results["test_traces_distribution"][index] ? model.results["test_traces_distribution"][index][1] : 0;
        }),
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
    ],
  }
  return <Bar options={options} data={data} />
}
