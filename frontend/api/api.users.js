import axios from "axios"

const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_USERS}`,
  withCredentials: true,
})

// export const getUsers = (page) => {
//     return api.get(`users?page=${page}`)
// }

export const getUsers = (page = 0, limit = 0) => {
  return api.get(`users?page=${page}&limit=${limit}`)
}

export const getUser = (id) => {
  return api.get(`users/${id}`)
}

export const delUsers = (id) => {
  return api.delete(`users/${id}`)
}
