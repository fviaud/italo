from sklearn.preprocessing import MinMaxScaler
from collections import Counter
from sklearn.metrics.cluster import v_measure_score,davies_bouldin_score
from agilkia import TraceSet
import copy
import pickle
from .preprocessing import prepare_dataset,dataframe_to_contiguous
from .model import training,inference
from .metrics import get_traces_test_distribution_per_cluster,get_events_distribution
from .utils import prune_event_distribution
def clustering_pipeline(model_name,execution_traces_dict,test_traces_dict):

    tests_not_empty = True
    if test_traces_dict is None or len(test_traces_dict.keys()) == 0:
        tests_not_empty = False
        test_clusters = None

    traceset_global, global_data, execution_data, test_data=prepare_dataset(execution_traces_dict,test_traces_dict)
    # Now cluster the traces
    clusterer,normalizer=training(model_name,traceset_global,copy.deepcopy(global_data))
    global_clusters=inference(clusterer,normalizer,dataframe_to_contiguous(global_data)).tolist()
    num_clusters=max(global_clusters)+1
    execution_clusters=global_clusters[:len(execution_data)]
    test_clusters=global_clusters[len(execution_data):]
    # execution_clusters=inference(clusterer,normalizer,dataframe_to_contiguous(execution_data)).tolist()

    if tests_not_empty:
        test_clusters=global_clusters[len(execution_data):]
        # test_clusters = clusterer.predict(normalizer.transform(dataframe_to_contiguous(test_data)))


    execution_traces_distribution,test_traces_distribution=get_traces_test_distribution_per_cluster(num_clusters,execution_clusters,test_clusters)
    events_distribution=get_events_distribution(global_data,num_clusters,traceset_global)
    #computing score Davies-Bouldin Score, we could implement later ground truth labels with v-measure
    # try:
    #     score=v_measure_score(execution_clusters,traceset_execution.get_meta('ground_truth_labels')).item()
    # except:
    score=davies_bouldin_score(global_data,global_clusters).item()
    score=1/score
    events_distribution = prune_event_distribution(events_distribution, num_clusters)
   
    
    # return {"score" : score,
    #         "execution_traces_distribution":execution_traces_distribution,
    #         "test_traces_distribution":test_traces_distribution,
    #         "execution_clusters":execution_clusters,
    #         "events_distribution":events_distribution,
    #         "clustering_model_serialized":pickle.dumps(clusterer),
    #         "normalizer_model_serialized":pickle.dumps(normalizer),
    #         "num_clusters":num_clusters}
    return {"score" : score,
             "execution_traces_distribution":execution_traces_distribution,
             "test_traces_distribution":test_traces_distribution,
             "execution_clusters":execution_clusters,
             "test_clusters":test_clusters,
             "events_distribution":events_distribution,
             "clustering_model_serialized":None,
             "normalizer_model_serialized":None,
             "num_clusters":num_clusters}


if __name__=='__main__':
    import json
    from sklearn.cluster import MeanShift,KMeans
    # filepath1='../data/executiontraces.json'
    # filepath2='../data/testtraces.json'
    filepath1 = '../data/execution_traces_v1.json'
    filepath2 = '../data/test_traces_v1.json'
    with open(filepath1) as json_file:
        data1 = json.load(json_file)
    with open(filepath2) as json_file:
        data2 = json.load(json_file)

    # print(clustering(KMeans,data1,data2))

    # clustering(KMeans,data1,data2)

    with open(filepath1) as json_file:
        data1 = json.load(json_file)
    with open(filepath2) as json_file:
        data2 = json.load(json_file)


    # print(clustering(MeanShift,data1,data2))


