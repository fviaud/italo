import React from "react"
import Router from "next/router"
import Link from "next/link"

import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemText from "@mui/material/ListItemText"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import IconButton from "@mui/material/IconButton"
import Typography from "@mui/material/Typography"
import Stack from "@mui/material/Stack"
import Divider from "@mui/material/Divider"
import EditIcon from "@mui/icons-material/Edit"
import Chip from "@mui/material/Chip"
import PlayArrowIcon from "@mui/icons-material/PlayArrow"

import Breadcrumbs from "@mui/material/Breadcrumbs"
// import Link from '@material-ui/core/Link';

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

import { notifMessage } from "components/utils/notif"
import MenuModels from "./menu"
import TableGeneration from "./table"

import { updateStatusJobs } from "api/api.jobs"

function handleClick(event) {
  event.preventDefault()
  console.info("You clicked a breadcrumb.")
}

function Item({ project, job, isLoading, isError, tab }) {
  if (isLoading) return <Spinner />
  if (isError) return <Error />

  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
        <Stack direction="row" spacing={2}>
          <IconButton edge="end" onClick={() => Router.back()}>
            <ArrowBackIcon />
          </IconButton>
          <Box sx={{ p: 1 }}>
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="" onClick={handleClick}>
                <Typography color="textPrimary">{project?.name}</Typography>
              </Link>
              <Typography color="textPrimary">{job?.name}</Typography>
            </Breadcrumbs>
          </Box>
        </Stack>
        {job?.status === "in progress" ? (
          <Chip size="small" label="In process" />
        ) : (
          <Box>
            <Link href={`/projects/${job.project}/jobs/${job._id.$oid}/edit`} style>
              <Button variant="text" startIcon={<EditIcon />}>
                Edit
              </Button>
            </Link>
          </Box>
        )}
      </Box>

      {job?.status !== "completed" && (
        <Box sx={{ mt: 1 }}>
          <List>
            <ListItem>
              <ListItemText
                primary={<Typography color="secondary">{job.name}</Typography>}
                secondary={<Typography>{job.description}</Typography>}
              />
            </ListItem>
          </List>
          <Divider />
          <Box sx={{ paddingTop: 2 }}>
            <Button
              color="primary"
              variant="text"
              startIcon={<PlayArrowIcon />}
              onClick={async () => {
                try {
                  const formData = new FormData()
                  formData.append("status", "in progress")
                  await updateStatusJobs(job._id.$oid, formData)
                  notifMessage("success", "Launch Benchmark with success")
                } catch (error) {
                  notifMessage("error", "Failed to Launch Benchmark")
                }
                Router.back()
              }}>
              Launch benchmark to wiew result
            </Button>
          </Box>
        </Box>
      )}
      <Box sx={{ mt: 5 }}>
        {job?.status === "completed" && <MenuModels job={job} tab={tab} />}
        {job?.status === "completed" && <TableGeneration job={job} />}
      </Box>
    </>
  )
}

export default Item
