import * as React from "react"
import Box from "@mui/material/Box"
import Card from "@mui/material/Card"
import { CardHeader } from "@mui/material"
import Link from "next/link"

import CardContent from "@mui/material/CardContent"

import Typography from "@mui/material/Typography"
import Avatar from "@mui/material/Avatar"
import { Button, CardActionArea, CardActions } from "@mui/material"

export default function OutlinedCard({ item }) {
  return (
    <Box sx={{ minWidth: 275 }}>
      <Card variant="outlined">
        <Link href={item.path} component={CardActionArea} style>
          <CardActionArea>
            <React.Fragment>
              <CardHeader
                avatar={<Avatar sx={{ bgcolor: item.color ? item.color : "#bdbdbd"}}>{item.icon}</Avatar>}
                title={
                  <Typography variant="h5" component="div">
                    {item.name}
                  </Typography>
                }
              />
              <CardContent>
                <Typography variant="h4">{item.data}</Typography>
              </CardContent>
              {/* <CardActions>
            <Button size="small">Learn More</Button>
          </CardActions> */}
            </React.Fragment>
          </CardActionArea>
        </Link>
      </Card>
    </Box>
  )
}
