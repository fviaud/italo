import * as React from "react"
import Link from "next/link"
import AppBar from "@mui/material/AppBar"
import Box from "@mui/material/Box"
import Toolbar from "@mui/material/Toolbar"
import IconButton from "@mui/material/IconButton"
import Typography from "@mui/material/Typography"
import Menu from "@mui/material/Menu"
import MenuIcon from "@mui/icons-material/Menu"
import Container from "@mui/material/Container"
import Avatar from "@mui/material/Avatar"
import Button from "@mui/material/Button"
import Tooltip from "@mui/material/Tooltip"
import MenuItem from "@mui/material/MenuItem"
import GlobalStyles from "@mui/material/GlobalStyles"
import Footer from "components/footer"
import { useQueryClient } from "react-query"
import { logout } from "api/api.auth"
import ScrollTop from "components/ScrollTop"
import Router from "next/router"
import Image from "next/image"
import imgOrange from "public/favicon.ico"
const pages = [
  { name: "Dashboard", link: "/dashboard" },
  { name: "Projects", link: "/projects" },
  { name: "Users", link: "/users" },
  { name: "Settings", link: "/settings" },
  { name: "Tutorials", link: "/tutorials" },
  { name: "About", link: "/about" },
]
const settings = ["Logout"]

function stringAvatar(name) {
  return {
    children: `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`,
  }
}

const ResponsiveAppBar = ({ children }) => {
  const queryClient = useQueryClient()
  const curentUser = queryClient.getQueryData("user")
  const [anchorElNav, setAnchorElNav] = React.useState(null)
  const [anchorElUser, setAnchorElUser] = React.useState(null)

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget)
  }
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget)
  }
  const handleCloseNavMenu = () => {
    setAnchorElNav(null)
  }
  const handleCloseUserMenu = () => {
    setAnchorElUser(null)
  }

  const handleSignout = async (setting) => {
    switch (setting.setting) {
      case "Logout":
        await logout()
        Router.push("/login")
        break
      default:
        break
    }
    setAnchorElUser(null)
  }

  return (
    <>
      <GlobalStyles
        styles={{
          body: {
            margin: 0,
            height: "100vh",
          },
          "#__next": {
            height: "100%",
            display: "flex",
            flexDirection: "column",
          },
        }}
      />
      <AppBar>
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Box
              sx={{ mr: 2, display: { xs: "none", md: "flex" } }}
              style={{ height: "40px", width: "40px", marginRight: "8px" }}>
              <Image src={imgOrange} alt="logo" />
            </Box>
            <Typography variant="h6" noWrap component="div" sx={{ mr: 2, display: { xs: "none", md: "flex" } }} color="primary">
              ITALO
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="primary">
                <MenuIcon />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: "block", md: "none" },
                }}>
                {pages.map((page, index) => (
                  <MenuItem key={index} onClick={handleCloseNavMenu}>
                    <Link href={page.link} style>
                      <Typography textAlign="center">{page.name}</Typography>
                    </Link>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}
              color="primary">
              ITALO
            </Typography>
            <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              {pages.map((page, index) => (
                <Link href={page.link} style key={index}>
                  <Button key={page} onClick={handleCloseNavMenu} sx={{ my: 2, color: "white", display: "block" }}>
                    {page.name}
                  </Button>
                </Link>
              ))}
            </Box>

            <Box sx={{ flexGrow: 0 }}>
              <Tooltip title="Open settings">
                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                  {/* <Avatar {...stringAvatar(curentUser.data.name)} /> */}
                  <Avatar />
                </IconButton>
              </Tooltip>
              <Menu
                sx={{ mt: "45px" }}
                id="menu-appbar"
                anchorEl={anchorElUser}
                anchorOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "right",
                }}
                open={Boolean(anchorElUser)}
                onClose={handleCloseUserMenu}>
                {settings.map((setting) => (
                  <MenuItem key={setting} onClick={() => handleSignout({ setting })}>
                    <Typography textAlign="center">{setting}</Typography>
                  </MenuItem>
                ))}
              </Menu>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
      <Container sx={{ flexGrow: 1 }}>
        <main>{children}</main>
      </Container>
      <ScrollTop />
      <Footer />
    </>
  )
}
export default ResponsiveAppBar
