import React from "react"

import Container from "@mui/material/Container"
import Stack from "@mui/material/Stack"
import Typography from "@mui/material/Typography"
import Box from "@mui/material/Box"
import Image from "next/image"

import tutorials from "public/tutorials.jpg"

function Tutorials() {
  return (
    <Container>
      <Stack spacing={2} mt={5}>
        <Typography variant="h5" sx={{ marginBottom: 2 }}>
          Overview
        </Typography>
        <Typography>
          The goal of the Philae project is to analyze customer logs (execution traces and test traces) with Machine Learning
          models to
          <ul>
            <li>Identify the need for regression tests</li>
            <li>Generate the missing tests</li>
          </ul>
        </Typography>
        <Typography>The pipeline of this project is summarized in the following image:</Typography>
        <Typography>
          Italo is a web application that allows teams of testers who have at their disposal customer logs to use the Philae
          pipeline in a simplified way without Machine Learning skills.
        </Typography>
        <Typography variant="h5" sx={{ marginBottom: 1, marginTop: 2 }}>
          Definition
        </Typography>
        <Typography>
          <ul>
            <li>Clustering: dividing a set of data into homogeneous packets</li>
            <li>Traces: sequence of API calls corresponding to a user session. Parameters are associated with each API call</li>
          </ul>
        </Typography>
        <Typography variant="h6" sx={{ marginBottom: 1, marginTop: 2 }}>
          1. How to identify missing tests with clustering?
        </Typography>
        <Typography>
          We assume that relevant clustering models can group execution traces and test traces into homogeneous clusters
          corresponding to the main behavior of the application. Behavioral clusters that contain execution traces (user sessions)
          but no tests are the behaviors not covered by the tests. In the following example, behavior 3 is not covered by tests.
        </Typography>
        <Box sx={{ position: "relative", height: "30vh", width: "35vw", margin: "auto" }}>
          <Image src={tutorials} alt="about-schematic" layout="fill" />
        </Box>
        <Typography variant="h6" sx={{ marginBottom: 1, marginTop: 2 }}>
          2. How can we be sure that the clustering models group the traces by behavior in a relevant way?
        </Typography>
        <Typography>
          Each clustering done has an accuracy score. Hence, we can look at the clustering model which has the best accuracy
          score. Sometimes a clustering can have a good accuracy score, but the clusters are not precise enough. it may be worth
          looking at a clustering that has a lower score but with a higher number of clusters
        </Typography>
        <Typography variant="h6" sx={{ marginBottom: 1, marginTop: 2 }}>
          3. How to generate the missing tests?
        </Typography>
        <Typography>
          We use generative models that come from natural language processing. Models are taught to complete traces from their
          beginning. We then generate all plausible paths for each behavior above a probability threshold
        </Typography>
        <Typography variant="h6" sx={{ marginBottom: 1, marginTop: 2 }}>
          4. How to be sure that generative models generate relevant tests?
        </Typography>
        <Typography>
          Tests are evaluated on several criteria:
          <ul>
            <li>Usage coverage</li>
            <li>Diversity</li>
          </ul>
        </Typography>
      </Stack>
    </Container>
  )
}

export default Tutorials
