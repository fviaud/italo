import React from "react"
import { useQuery } from "react-query"
import Link from "next/link"

import Stack from "@mui/material/Stack"
import Avatar from "@mui/material/Avatar"
import FolderIcon from "@mui/icons-material/Folder"
import Typography from "@mui/material/Typography"
import Tabs from "@mui/material/Tabs"
import Tab from "@mui/material/Tab"
import Divider from "@mui/material/Divider"
import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"

import { getProject } from "api/api.projects"
import { getUsers } from "api/api.users"

import Appbar from "components/layouts/appbar"
import Users from "components/users/items"
import { stringToHslColor } from "helpers"

function LinkTab(props) {
  return (
    <Link href={props.href}>
      <Tab component="a" {...props} sx={{ textTransform: "none" }} />
    </Link>
  )
}

function Index({ params, query }) {
  const { pid } = params

  const { isFetching, isError, data: PROJECT, error } = useQuery("project", () => getProject(pid), { retry: false })

  const page = query.page
  const fetchObjects = (page = 1) => getUsers(page, 10)

  const {
    isLoading: isLoadingUsers,
    isError: isErrorUsers,
    error: errorUsers,
    data: USERS,
    refetch,
  } = useQuery(["users", page], () => fetchObjects(page), { keepPreviousData: true })

  const [value, setValue] = React.useState(2)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <Appbar>
      <Stack mt={2}>
        <Stack direction="row" spacing={2}>
          <Avatar sx={{ bgcolor: stringToHslColor(PROJECT?.data[0].name) }}>
            <FolderIcon />
          </Avatar>
          <Typography variant="h4" color="primary">
            {PROJECT?.data[0].name}
          </Typography>
        </Stack>
        <Stack>
          <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
            <Tabs value={value} onChange={handleChange} aria-label="nav tabs example">
              <LinkTab key={0} label="Jobs" href={`/projects/${pid}/jobs`} />
              <LinkTab key={1} label="Traces" href={`/projects/${pid}/traces`} />
              <LinkTab key={2} label="Members" href={`/projects/${pid}/members`} />
            </Tabs>
            <Link href={``} style>
              <Button variant="text" disabled startIcon={<AddCircleOutlineIcon />}>
                Add
              </Button>
            </Link>
          </Box>
          <Divider />
          <main>
            <Users
              USERS={USERS}
              isLoading={isLoadingUsers}
              refetch={refetch}
              pid={pid}
              isError={isErrorUsers}
              error={errorUsers}
            />
            {/* <Todo message={"Todo"} /> */}
          </main>
        </Stack>
      </Stack>
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params, query: context.query } }
}

export default Index

Index.auth = { role: "admin" }
