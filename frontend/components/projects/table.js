import * as React from "react"
import { useRouter } from "next/router"
import { styled } from "@mui/material/styles"
import Table from "@mui/material/Table"
import TableBody from "@mui/material/TableBody"
import TableCell, { tableCellClasses } from "@mui/material/TableCell"
import TableContainer from "@mui/material/TableContainer"
import TableHead from "@mui/material/TableHead"
import TableRow from "@mui/material/TableRow"
import Typography from "@mui/material/Typography"
import IconButton from "@mui/material/IconButton"
import OfflineShareIcon from "@mui/icons-material/OfflineShare"
import Grid from "@mui/material/Grid"

import Box from "@mui/material/Box"

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    // backgroundColor: theme.palette.common.black,
    // color: theme.palette.common.black,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}))

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    // backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}))

function createData(Model, Diversity, Controllabilty, ExecutionTime) {
  return { Model, Diversity, Controllabilty, ExecutionTime }
}

export default function BasicTable({ project }) {
  const router = useRouter()

  const rows = project.generativeModels.map((model) =>
    createData(
      model.name,
      project.generativeModels.find((model2) => model2.name === model.name).results.coverage,
      project.generativeModels.find((model2) => model2.name === model.name).results.controllability,
      project.generativeModels.find((model2) => model2.name === model.name).results.execution_time
    )
  )

  return (
    <>
      <Grid container columns={{ md: 12 }}>
        <Grid item xs={6}>
          <Box mt={1} mb={2}>
            <Typography variant="h5">Generation</Typography>
          </Box>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo lobortis
            eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex, sit amet blandit leo
            lobortis eget.
          </Typography>
        </Grid>
        <Grid item xs={6}>
          <TableContainer>
            <Table sx={{ minWidth: 350 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <StyledTableCell></StyledTableCell>
                  <StyledTableCell align="right">Coverage</StyledTableCell>
                  <StyledTableCell align="right">Controllabilty</StyledTableCell>
                  <StyledTableCell align="right">ExecutionTime</StyledTableCell>
                  <StyledTableCell align="right"></StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <StyledTableRow key={row.Model} sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
                    <StyledTableCell component="th" scope="row">
                      {row.Model}
                    </StyledTableCell>
                    <StyledTableCell align="right">{row.Diversity}</StyledTableCell>
                    <StyledTableCell align="right">{row.Controllabilty}</StyledTableCell>
                    <StyledTableCell align="right">{row.ExecutionTime}</StyledTableCell>
                    <StyledTableCell align="right">
                      <IconButton
                        onClick={() => {
                          router.push(`${project._id.$oid}/${row.Model}`)
                        }}>
                        <OfflineShareIcon />
                      </IconButton>
                    </StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    </>
  )
}
