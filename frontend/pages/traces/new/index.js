import React from "react"
import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"

import Appbar from "components/layouts/appbarForm"
import Form from "components/traces/formTraces"

export default function Index() {
  return (
    <Appbar>
      {/* <Box sx={{ display: "flex", justifyContent: "space-between" }} pt={2}>
        <Typography variant="h4" color="primary">
          Add traces
        </Typography>
      </Box>
      <Divider /> */}
      <Form />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

Index.auth = { role: "admin" }
