from .preprocessing import load_from_dict
from agilkia import TraceSet,Event,Trace
import agilkia
import numpy as np
from random import randint

from .model import train,generate_tests_per_clusters
from .preprocessing import prepare_dataset
from .metrics import metrics
import time

def generation_pipeline(generative_model_name,clustering_model_binary,normalizer_model_binary, execution_traces_dict,test_traces_dict,execution_clusters,test_clusters,total_number_of_tests=30):

    start_time=time.time()
    traceset_global,X,y,ex =prepare_dataset(execution_traces_dict,test_traces_dict,execution_clusters,test_clusters)
    model=train(generative_model_name,ex,traceset_global,y)
    tests_per_clusters=generate_tests_per_clusters(generative_model_name,model,execution_clusters,test_clusters,total_number_of_tests)
    diversity,usage=metrics(traceset_global,tests_per_clusters)

    execution_time=round(time.time()-start_time,2)
    return { "tests_per_clusters":tests_per_clusters,
             "diversity":diversity,
             "usage":usage,
             "execution_time":execution_time
             }



