from .benchmark.BenchmarkManager import BenchmarkManager
def metrics(reference_traceset,tests_per_clusters):
    bm = BenchmarkManager(reference_traceset, dataset_type='scanette', nb_of_experiments=2,nb_of_traces_generated=[10, 20, 30])
    all_tests=[]
    for id_cluster,tests in tests_per_clusters.items():
        all_tests+=tests

    return round(bm.n_gram_coverage(all_tests),2),round(bm.usage(all_tests),2)
