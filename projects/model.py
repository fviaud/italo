from flask_mongoengine import MongoEngine
db = MongoEngine()

class Projects(db.Document):
    name= db.StringField(max_length=200)
    description= db.StringField(max_length=200)
    author= db.StringField(max_length=200)

    def to_json(self):
        return {"name": self.name}

class Jobs(db.Document):
    name= db.StringField(max_length=200)
    description= db.StringField(max_length=200)
    project=db.StringField(max_length=200)
    execution_traces= db.DynamicField()
    test_traces= db.DynamicField()
    models=db.DynamicField()
    generativeModels=db.DynamicField()
    nbScenarios=db.IntField()
    status= db.StringField(max_length=200)
    author= db.StringField(max_length=200)
   
    def to_json(self):
        return {"name": self.name}




