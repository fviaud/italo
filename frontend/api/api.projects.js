import axios from "axios"

const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_PROJECTS}`,
  withCredentials: true,
})

export const getProjects = (page = 0, limit = 0, fields = []) => {
  return api.get(`projects`, { params: { page, limit, fields } })
}
export const getProject = (id) => {
  return api.get(`projects/${id}`)
}

export const addProjects = (object) => {
  return api.post("projects", object)
}

export const updateProjects = (object) => {
  return api.patch("projects", object)
}

// export const updateStatusProjects = (id, object) => {
//   return api.patch(`projects/${id}/status`, object)
// }

export const delProjects = (id) => {
  return api.delete(`projects/${id}`)
}
