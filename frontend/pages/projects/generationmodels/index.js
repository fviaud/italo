import React from "react"

import { useQuery } from "react-query"
import Appbar from "components/layouts/appbar"
import Container from "@mui/material/Container"
import Generation from "components/generation/items"

import { getJob } from "api/api.jobs"

export default function DecisionTrees({ query }) {
  const { idJob, model } = query
  const { isLoading, isError, data: JOB } = useQuery("job", () => getJob(idJob), { retry: false })

  return (
    <Appbar>
      <Generation job={JOB?.data[0]} model={model} isLoading={isLoading} isError={isError} />
    </Appbar>
  )
}
export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

DecisionTrees.auth = { role: "admin" }
