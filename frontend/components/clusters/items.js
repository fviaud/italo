import React from "react"
import Router from "next/router"
import Box from "@mui/material/Box"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import IconButton from "@mui/material/IconButton"
import Breadcrumbs from "./breadcrumbs"
import Stack from "@mui/material/Stack"
import Menu from "./menu"

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

function items({ project, CLUSTER, TESTCLUSTER, query, page = 1,testPage = 1, isLoading, isError, job, pid, jid }) {
  const { idcluster, menu } = query

  if (isLoading) return <Spinner />
  if (isError) return <Error />

  return (
    <>
      <Stack direction="row" spacing={2} sx={{alignItems: "center", marginTop: 2}}>
        <IconButton sx={{marginTop: 0.5, marginBottom: 0.5}} edge="end" onClick={() => Router.push(`/projects/${project._id["$oid"]}/jobs/${job.data[0]._id["$oid"]}`)}>
          <ArrowBackIcon />
        </IconButton>
        <Box sx={{ p: 2 }}>
          <Breadcrumbs project={project} CLUSTER={CLUSTER} idcluster={idcluster} />
        </Box>
      </Stack>
      <Menu CLUSTER={CLUSTER} TESTCLUSTER={TESTCLUSTER} query={query} page={page} testPage={testPage} menu={menu} idcluster={idcluster} job={job} pid={pid} jid={jid} />
    </>
  )
}

export default items
