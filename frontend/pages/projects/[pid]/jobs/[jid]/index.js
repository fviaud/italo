import React from "react"
import Link from "next/link"
import { useQuery } from "react-query"
import Router from "next/router"

import Stack from "@mui/material/Stack"
import Typography from "@mui/material/Typography"
import Tabs from "@mui/material/Tabs"
import Tab from "@mui/material/Tab"
import Box from "@mui/material/Box"
import Divider from "@mui/material/Divider"
import IconButton from "@mui/material/IconButton"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import Breadcrumbs from "@mui/material/Breadcrumbs"
import Chip from "@mui/material/Chip"
import Button from "@mui/material/Button"
import Switch from "@mui/material/Switch"
import FormControlLabel from "@mui/material/FormControlLabel"
import EditIcon from "@mui/icons-material/Edit"

import Job from "components/jobs/item"
import Chart from "components/charts"
import TableGeneration from "components/jobs/table"
import Spinner from "components/utils/spinner"

import Appbar from "components/layouts/appbar"
import { getProject } from "api/api.projects"
import { getJob } from "api/api.jobs"
import { findUntested } from "helpers";

function LinkTab(props) {
  return (
    <Link href={props.href}>
      <Tab component="a" {...props} sx={{ textTransform: "none" }} />
    </Link>
  )
}

function handleClick(event) {
  event.preventDefault()
  console.info("You clicked a breadcrumb.")
}

export default function Index({ params, query }) {
  const { pid, jid } = params
  const { tab } = query

  const {
    isFetching: isFetchingProject,
    isError: isErrorProject,
    data: PROJECT,
    error: errorProjects,
  } = useQuery("project", () => getProject(pid), { retry: false })

  const { isFetching: isFetchingJob, isError, data: JOB, error } = useQuery("job", () => getJob(jid), { retry: false })

  if (error?.response.status === 401) Router.push("/login")

  const [value, setValue] = React.useState(parseInt(tab) || 0)
  const [unTestedOnly, setUnTestedOnly] = React.useState(false)

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleUntestedOnlySwitch = (event) => {
    event.stopPropagation();
    setUnTestedOnly(state => !state);
  }

  return (
    <Appbar>
      <Stack spacing={2} mt={2}>
        <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
          <Stack direction="row" spacing={2}>
            <IconButton edge="end" onClick={() => Router.push(`/projects/${pid}/jobs/`)}>
              <ArrowBackIcon />
            </IconButton>
            <Box sx={{ p: 1 }}>
              <Breadcrumbs aria-label="breadcrumb">
                <Link color="inherit" href="" onClick={handleClick}>
                  <Typography color="textPrimary">{PROJECT?.data[0].name}</Typography>
                </Link>
                <Typography color="textPrimary">{JOB?.data[0].name}</Typography>
              </Breadcrumbs>
            </Box>
          </Stack>
          <Box>
            <Link href={`/projects/${JOB?.data[0].project}/jobs/${JOB?.data[0]._id.$oid}/edit`} style>
              <Button variant="text" startIcon={<EditIcon />}>
                Edit
              </Button>
            </Link>
          </Box>
        </Box>
        <Typography variant="h5" color={"primary"}>
          Identification of Missing Tests
        </Typography>
        <Typography>
          Clustering allows you to identify the missing tests. Various clustering models (Machine Learning) have split your
          execution traces and test traces into groups of similarity. These groups are called clusters and can be assimilated to
          specific behaviors of your application. In the chart below, you can see the volume of execution traces and test traces
          belonging to each cluster. If a cluster contains execution traces but no test traces, you could need to test this
          specific behavior. Click on the bars to see details. Each clustering model has a score. The higher the score is, the
          better the model has split the traces in a relevant way. But be careful, nothing is better than domain-knowledge !
        </Typography>
      </Stack>

      {JOB && (
        <>
          <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
            <Tabs value={value} onChange={handleChange} aria-label="nav tabs example">
              {JOB?.data[0].models.map((item, index) => (
                <LinkTab
                  key={index}
                  label={
                    <div>
                      {item.name}
                      {item.results && <Chip size="small" label={Math.round(item.results.score * 100)} />}
                    </div>
                  }
                  href={`/projects/${pid}/jobs/${jid}?tab=${index}`}
                />
              ))}
            </Tabs>
          </Box>
          <Divider />
          <main>
            {JOB?.data[0]["models"][value].results ? (
              <Stack spacing={3}>
                <>
                  <Box sx={{display:"flex",justifyContent:"flex-end"}}>
                    <FormControlLabel
                      control={<Switch color="primary" checked={unTestedOnly} onClick={handleUntestedOnlySwitch}/>}
                      label={<Typography>Show only untested cluster</Typography>}
                      labelPlacement="start"
                    />
                  </Box>
                  <Chart job={JOB?.data[0]} model={JOB?.data[0]["models"][value]} indexModel={value} unTested={unTestedOnly ? findUntested(JOB?.data[0]["models"][value]) : false}/>
                  <Divider />
                  {JOB?.data[0].status === "completed" && <TableGeneration job={JOB?.data[0]} />}
                </>
              </Stack>
            ) : (
              <Box mt={5} textAlign="center">
                <Typography variant="h6" color={"primary"}>
                  In process
                </Typography>
              </Box>
            )}
          </main>
        </>
      )}
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params, query: context.query } }
}

Index.auth = { role: "admin" }
