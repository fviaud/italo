import React from "react"
import Router, { useRouter } from "next/router"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import IconButton from "@mui/material/IconButton"
import DeleteIcon from "@mui/icons-material/Delete"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Chip from "@mui/material/Chip"
import Pagination from "@mui/material/Pagination"
import AccountCircleIcon from "@mui/icons-material/AccountCircle"

import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"
import { delUsers } from "api/api.users"
import { stringToHslColor } from "helpers"

export default function Items({ USERS, isLoading, page = 0, isError }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />
  return (
    <Stack mt={2}>
      {USERS.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {USERS.data.results.map((user, index) => (
          <ListItemButton dense disableRipple key={index} onClick={() => Router.push(`/users/${user._id.$oid}`)}>
            <ListItem
              secondaryAction={
                <IconButton
                  edge="end"
                  onClick={async (e) => {
                    e.stopPropagation()
                    if (window.confirm("Do you wand delete this object ?")) {
                      try {
                        await delUsers(user._id.$oid)
                        notifMessage("success", "Delete user with success !")
                      } catch (error) {
                        notifMessage("error", "Failed to delete user!")
                      }
                    }
                  }}>
                  <DeleteIcon />
                </IconButton>
              }>
              <ListItemAvatar>
                <Avatar sx={{bgcolor: stringToHslColor(user.name)}}>
                  <AccountCircleIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={<>{user.name}</>} secondary={user.email} />
            </ListItem>
          </ListItemButton>
        ))}
      </List>
      {(USERS.data.totalPages > 1 || USERS.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={USERS.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/users/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
