from sklearn.cluster import MeanShift,KMeans
from sklearn.preprocessing import MinMaxScaler
from .utils import DBSCANWrapper,SpectralClusteringWrapper


def training(model_name,traceset_global,global_data):
    print(model_name.lower())
    if model_name.lower() in ['kmeans','meanshift','kmeans30','kmeans100','dbscan','spectralclustering']:
        if model_name.lower()=='kmeans':
            clusterer=KMeans(n_clusters=8)
            print('initialize kmeans')
        elif model_name.lower()=='meanshift':
            clusterer=MeanShift()
            print('initialize meanshift')
        elif model_name.lower()=='kmeans30':
            clusterer=KMeans(n_clusters=30)
        elif model_name.lower()=='kmeans100':
            clusterer=KMeans(n_clusters=100)
        elif model_name.lower() == 'dbscan':
            clusterer = DBSCANWrapper()
            print('initialize dbscan')
        elif model_name.lower()=='spectralclustering':
            clusterer = SpectralClusteringWrapper(n_clusters=8, assign_labels='discretize', random_state=0)
        else:
            raise ValueError('wrong value for model name')

        normalizer=MinMaxScaler()
        num_clusters = traceset_global.create_clusters(global_data, algorithm=clusterer, normalizer=normalizer)

    else:
        raise ValueError('wrong value for model name')
    return clusterer,normalizer



def inference(clusterer,normalizer,data):

    return clusterer.predict(normalizer.transform(data))