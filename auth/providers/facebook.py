from flask import redirect
from flask_login import login_user
from authlib.integrations.flask_client import OAuth

from .model import Users

import os
import json

NAME="facebook"
SCOPE='email'
CONF_URL='https://www.facebook.com/.well-known/openid-configuration/'
CONF_TOKEN='https://graph.facebook.com/oauth/access_token'
CONF_USERINFO='https://graph.facebook.com/me?fields=id,name,email,picture{url}'

def facebook_routes(app):
    oauth = OAuth(app)
    oauth.register(
        name=NAME,
        access_token_url=CONF_TOKEN,
        server_metadata_url=CONF_URL,
        client_kwargs={'scope': SCOPE},
    )

    @app.route('/api/auth/login/facebook')
    def login_facebook():
        return oauth.facebook.authorize_redirect(os.environ.get('FACEBOOK_REDIRECT_URI'))

    @app.route('/api/auth/callback/facebook')
    def auth_facebook():
        token = oauth.facebook.authorize_access_token()
        resp = oauth.facebook.get(CONF_USERINFO)
        userinfo = resp.json()
        if userinfo:
            user=Users.objects(email=userinfo['email']).first()
            if not user:
                user = Users(
                    email=userinfo['email'],
                    name=userinfo['name'])
                user.save()
            user.id = user.to_json()["id"]
            login_user(user)
        return redirect('/')
