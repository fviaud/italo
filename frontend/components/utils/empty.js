import React from "react"
import SimpleReturnPage from "./simpleReturnPage"

export default function CircularIndeterminate() {
  return (
    <SimpleReturnPage message={"Is empty"}/>
  )
}
