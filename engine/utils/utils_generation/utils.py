from itertools import cycle


def t_by_clusters(test_numbers,initial_distribution_dict):
    distribution_ratios={}
    total_traces_number=0

    #Computing the total amount of traces within the clusters
    for k,v in initial_distribution_dict.items():
        total_traces_number+=v
    #Computing the ratio of traces in each cluster with respect to the total number of traces
    for k,v in initial_distribution_dict.items():
        distribution_ratios[k]=v/total_traces_number


    #Creating a cycle list with the range of clusters
    # https://docs.python.org/fr/3/library/itertools.html

    clusters=[]
    for k,v in initial_distribution_dict.items():
        clusters.append(k)
    cluster_cycle=cycle([elt for elt in clusters])

    #Initialize the tests distribution dict
    tests_distribution_dict={}
    for k,v in initial_distribution_dict.items():
        tests_distribution_dict[k]=0

    remaining_tests=test_numbers #number of tests to dispatch into cluster, we want at least one test in each cluster
    for cluster in cluster_cycle:
        if remaining_tests<=0:
            break
        if max(distribution_ratios[cluster]*test_numbers,1)> tests_distribution_dict[cluster]:
            tests_distribution_dict[cluster]+=1
            remaining_tests-=1
    return tests_distribution_dict


if __name__=='__main__':
    initial_distribution_dict={"0":25,"1":12,"2":30,"3":1}
    test_number=30
    d=t_by_clusters(test_number,initial_distribution_dict)
    print(d)
