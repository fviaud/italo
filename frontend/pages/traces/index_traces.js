import React, { useState } from "react"
import { useQuery } from "react-query"

import Box from "@mui/material/Box"
import Typography from "@mui/material/Typography"
import Divider from "@mui/material/Divider"

import Traces from "components/traces/items"
import Appbar from "components/layouts/appbar"
import Link from "next/link"

import { getTraces } from "api/api.traces"
import Form from "components/traces/form"

import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import Button from "@mui/material/Button"

export default function Index({ query }) {
  const [upLoading, setUpLoading] = useState(false)
  const page = query.page
  const fetchObjects = (page = 1) => getTraces(page, 10)
  const {
    isLoading,
    isError,
    data: TRACES,
    refetch,
  } = useQuery(["traces", page], () => fetchObjects(page), { keepPreviousData: true })

  return (
    <Appbar>
      <Box sx={{ display: "flex", justifyContent: "space-between" }} mt={2}>
        <Typography variant="h4" color="primary">
          Traces
        </Typography>
        {/* <Form refetch={refetch} setUpLoading={setUpLoading} upLoading={upLoading} /> */}
        <Link href={"/traces/new/"} style>
          <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
            Add
          </Button>
        </Link>
      </Box>
      <Divider />
      <Traces TRACES={TRACES} page={page} isLoading={isLoading} isError={isError} refetch={refetch} upLoading={upLoading} />
    </Appbar>
  )
}
export function getServerSideProps(context) {
  return { props: { query: context.query } }
}

Index.auth = { role: "admin" }
