import React from "react"
import Router from "next/router"
import Link from "next/link"

import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemText from "@mui/material/ListItemText"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import IconButton from "@mui/material/IconButton"
import Typography from "@mui/material/Typography"
import Stack from "@mui/material/Stack"
import Divider from "@mui/material/Divider"
import EditIcon from "@mui/icons-material/Edit"
import Chip from "@mui/material/Chip"
import PlayArrowIcon from "@mui/icons-material/PlayArrow"

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

import { updateStatusProjects } from "api/api.projects"
import { notifMessage } from "components/utils/notif"
import MenuModels from "./menu"
import TableGeneration from "./table"
import MenuGeneration from "./menuGeneration"

function Item({ project, dataset, isFetching, isError }) {
  if (isFetching) return <Spinner />
  if (isError) return <Error />

  return (
    <>
      <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
        <Stack direction="row" spacing={2}>
          <IconButton edge="end" onClick={() => Router.push("/projects")}>
            <ArrowBackIcon />
          </IconButton>
        </Stack>
        {project?.status === "in progress" ? (
          <Chip size="small" label="In process" />
        ) : (
          <Box>
            <Button
              color="primary"
              variant="text"
              startIcon={<PlayArrowIcon />}
              onClick={async () => {
                try {
                  const formData = new FormData()
                  formData.append("status", "in progress")
                  await updateStatusProjects(project._id.$oid, formData)
                  notifMessage("success", "Launch Benchmark with success")
                } catch (error) {
                  notifMessage("error", "Failed to Launch Benchmark")
                }
                Router.push("/projects")
              }}>
              Launch benchmark
            </Button>
            <Link href={`/projects/edit/${project._id.$oid}`} style>
              <Button variant="text" startIcon={<EditIcon />}>
                Edit
              </Button>
            </Link>
          </Box>
        )}
      </Box>
      <List>
        <ListItem>
          <ListItemText primary={<Typography color="secondary">Name</Typography>} secondary={project.name} />
        </ListItem>
        <ListItem>
          <ListItemText primary={<Typography color="secondary">Description</Typography>} secondary={project.description} />
        </ListItem>
        <ListItem>
          <ListItemText primary={<Typography color="secondary">Visibility</Typography>} secondary={project.public ? "Public" : "Private"} />
        </ListItem>
        <ListItem>
          <ListItemText primary={<Typography color="secondary">Dataset</Typography>} secondary={dataset.name} />
        </ListItem>

        <ListItem>
          <ListItemText
            primary={<Typography color="secondary">Clustering</Typography>}
            secondary={project.models.map((item, index) => (
              <span key={index}>
                {item.name}
                <br />
              </span>
            ))}
          />
        </ListItem>
        <ListItem>
          <ListItemText
            primary={<Typography color="secondary">Generation</Typography>}
            secondary={project.generativeModels?.map((item, index) => (
              <span key={index}>
                {item.name}
                <br />
              </span>
            ))}
          />
        </ListItem>
        <ListItem>
          <ListItemText
            primary={<Typography color="secondary">Number Of generated scenarios per cluster</Typography>}
            secondary={project?.nbScenarios}
          />
        </ListItem>
      </List>
      {project?.status === "completed" && <MenuModels project={project} />}
      {project?.status === "completed" && <TableGeneration project={project} />}
    </>
  )
}

export default Item
