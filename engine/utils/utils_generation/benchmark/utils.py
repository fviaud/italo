from statistics import mean,stdev
import math
from collections import Counter
def error_95(samples):
    error = 1.96 * stdev(samples) / math.sqrt(len(samples))
    return round(error,2)

def nb_traces_by_cluster(total_traces,reference_traceset,clusters):

    counts = Counter(clusters)
    count_pairs = sorted(list(counts.items()), key=lambda x: x[1], reverse=True)
    ratios = [x[1] / len(reference_traceset) for x in count_pairs]
    numbers=[[elt[0],round(total_traces/max(clusters)+1)] for elt in count_pairs]

    # for i, ratio in enumerate(ratios):
    #     numbers[i][1] = round(ratio * total_traces)
    # step=0
    # for i in range(total_traces - sum([elt[1] for elt in numbers])):
    #     numbers[step][1] += 1
    #     step=(step+1)%len(numbers)
    return numbers

def concatenate_results(results):
    new_results=[]
    for elt in results:
        new_results.append(elt[0][1:])
    return new_results

def best_n_traces_for_specific_cluster(token_cluster, nb_traces, inference_function, idx2str, end_token, length=20,
                                       action_prob=0.0005, path_prob=0.0005):
    results = []

    def depth_first_search(prefix, prob):

        # print(indent + ",".join([ev.action for ev in prefix]))
        if len(prefix) >= length:
            results.append((prefix, prob))
        else:
            proba = inference_function(prefix)
            for i, p in enumerate(proba):
                if p >= action_prob and prob * p >= path_prob:
                    action = idx2str[i]
                    if action == end_token:
                        results.append((prefix, prob))
                    else:
                        depth_first_search(prefix + [action], prob * p)

    depth_first_search([token_cluster], 1.0)
    results.sort(key=lambda tup: tup[1], reverse=True)

    return results[:nb_traces]