import axios from "axios"

const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_TRACES}`,
  withCredentials: true,
})

export const getStores = (page, limit = 0, pid, fields = []) => {
  return api.get(`traces/${pid}/stores`, { params: { page, limit, fields } })
}

export const delStores = (id) => {
  return api.delete(`traces/stores/${id}`)
}
