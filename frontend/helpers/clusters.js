export const findUntested = (data_model = {}) => {
    if(!data_model.results || !Array.isArray(data_model.results["test_traces_distribution"]) || typeof data_model == "undefined") return false
    const results = [];
    for(let cluster of data_model.results["test_traces_distribution"]){
        if(cluster[1] === 0) results.push(cluster[0])
    }
    return results;
}