import React from "react"
import Router from "next/router"

import { useQueryClient } from "react-query"

import TextField from "@mui/material/TextField"
import Box from "@mui/material/Box"
import { LoadingButton } from "@mui/lab"
import Stack from "@mui/material/Stack"
import Button from "@mui/material/Button"
import Paper from "@mui/material/Paper"
import MenuItem from "@mui/material/MenuItem"
import Typography from "@mui/material/Typography"
import FormLabel from "@mui/material/FormLabel"
import Input from "@mui/material/Input"

import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { notifMessage } from "components/utils/notif"

import { Formik, Form } from "formik"
import * as Yup from "yup"
import { addDatasets, updateDatasets } from "api/api.datasets"
import { addTraces } from "api/api.traces"
import HelpTooltip from "components/helpTooltip"

const manageLegacyValue = (dataset, TRACES, execution = true) => {
  try {
    if (Array.isArray(dataset[execution ? "execution_traces" : "test_traces"])) {
      const traces = dataset[execution ? "execution_traces" : "test_traces"].map((tid) =>
        TRACES.data.results.find((trace) => trace._id.$oid === tid._id.$oid)
      )
      return traces
    } else {
      const trace = TRACES.data.results.find(
        (trace) => trace._id.$oid === dataset[execution ? "execution_traces" : "test_traces"]._id.$oid
      )
      if (trace) {
        return [trace]
      }
    }
    return []
  } catch (error) {
    return []
  }
}

function FormDataset({ dataset, STORES, isFetching, isError, refetch }) {
  const queryClient = useQueryClient()

  if (isFetching) return <Spinner />
  if (isError) return <Error />

  const curentSession = queryClient.getQueryData("session")

  const initialValues = {
    name: dataset?.name ?? "",
    description: dataset?.description ?? "",
    execution_traces: dataset?.execution_traces ?? [],
    test_traces: dataset?.test_traces ?? [],
    public: dataset?.public ?? true,
  }
  const validationSchema = Yup.object({
    name: Yup.string().required("Is required"),
    description: Yup.string().required("Is required"),
    execution_traces: Yup.array().min(1).required("Is required"),
    test_traces: Yup.array().min(1).required("Is required"),
    public: Yup.boolean(),
  })

  const onSubmit = async (values) => {
    const formData = new FormData()
    if (dataset?._id) {
      formData.append("id", dataset._id.$oid)
    }
    formData.append("name", values.name)
    formData.append("author", curentSession.data._user_id)
    formData.append("description", values.description)
    formData.append("execution_traces", JSON.stringify(values.execution_traces))
    formData.append("test_traces", JSON.stringify(values.test_traces))
    formData.append("public", values.public)
    try {
      await (dataset?._id ? updateDatasets(formData) : addDatasets(formData))
      notifMessage("success", `${dataset?._id ? "Update " : "Add "} with success !`)
    } catch (error) {
      notifMessage("error", "Failed to add or update dataset!")
    }
    Router.back()
  }

  const onSubmitFile = async (e) => {
    const formData = new FormData()
    formData.append("trace", file.files[0])
    formData.append("author", curentSession.data._user_id)
    formData.append("public", true)
    try {
      await addTraces(formData)
      notifMessage("success", "Add trace with success !")
      refetch()
    } catch (error) {
      console.log(error)
      notifMessage("error", "Failed to add trace !")
    }
  }

  return (
    <Stack mt={2}>
      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Stack direction="row" spacing={2} alignItems="center">
          {/* <IconButton edge="end" onClick={() => Router.back()}>
            <ArrowBackIcon />
          </IconButton> */}
          <Typography variant="h6" color="primary">
            {dataset ? "Modify dataset" : "New dataset"}
          </Typography>
        </Stack>
      </Box>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
        {(formik) => (
          <Form autoComplete="off" noValidate>
            <Stack spacing={2} sx={{ pt: 2 }}>
              <Paper variant="outlined">
                <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                  <FormLabel>
                    <Typography variant="h6" style={{ fontWeight: 600 }}>
                      Dataset informations
                    </Typography>
                  </FormLabel>
                </Box>
                <Box sx={{ p: 2, paddingTop: 0 }}>
                  <Stack spacing={2} sx={{ mt: 2 }}>
                    <TextField
                      margin="dense"
                      id="name"
                      label="Name"
                      fullWidth
                      required
                      variant="outlined"
                      value={formik.values.name}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.name && formik.errors.name)}
                      helperText={formik.touched.name && formik.errors.name}
                    />
                    <TextField
                      id="description"
                      name="description"
                      label="Description"
                      fullWidth
                      multiline
                      maxRows={4}
                      required
                      value={formik.values.description}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      error={Boolean(formik.touched.description && formik.errors.description)}
                      helperText={formik.touched.description && formik.errors.description}
                    />
                    {/* <Box sx={{ p: 1, display: "flex", alignItems: "center" }}>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={formik.values.public ?? true}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            name="public"
                          />
                        }
                        sx={{ m: 0 }}
                        label="Private"
                        labelPlacement="start"
                      />
                      <Typography sx={{ paddingLeft: 1 }}>Public</Typography>
                    </Box> */}
                  </Stack>
                </Box>
              </Paper>
              <Paper variant="outlined">
                <Box sx={{ p: 2, paddingBottom: 0, paddingTop: 1, display: "flex" }}>
                  <FormLabel>
                    <Typography variant="h6" style={{ fontWeight: 600 }}>
                      Traces
                    </Typography>
                  </FormLabel>
                  <Box sx={{ flexGrow: 1 }} />
                  {/* <label htmlFor={`file`}>
                    <Button color="secondary" variant="text" startIcon={<UploadIcon />} component="span">
                      Upload a new trace
                    </Button>
                  </label> */}
                </Box>
                <Box sx={{ p: 2, paddingTop: 0 }}>
                  <Stack spacing={2} sx={{ mt: 2 }}>
                    {STORES?.data?.results && (
                      <>
                        <Box sx={{ display: "flex" }}>
                          <TextField
                            id="execution_traces"
                            name="execution_traces"
                            label="execution_traces"
                            select
                            SelectProps={{ multiple: true }}
                            required
                            value={formik.values.execution_traces}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            fullWidth
                            error={Boolean(formik.touched.execution_traces && formik.errors.execution_traces)}
                            helperText={formik.touched.execution_traces && formik.errors.execution_traces}>
                            {STORES?.data.results.map((value, index) => (
                              <MenuItem key={index} value={value._id.$oid}>
                                {value.name}
                              </MenuItem>
                            ))}
                          </TextField>
                          <HelpTooltip label={"help text 5"} />
                        </Box>
                        <Box sx={{ display: "flex" }}>
                          <TextField
                            id="test_traces"
                            name="test_traces"
                            label="test_traces"
                            select
                            SelectProps={{ multiple: true }}
                            required
                            value={formik.values.test_traces}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            fullWidth
                            error={Boolean(formik.touched.test_traces && formik.errors.test_traces)}
                            helperText={formik.touched.test_traces && formik.errors.test_traces}>
                            {STORES?.data.results.map((value, index) => (
                              <MenuItem key={index} value={value._id.$oid}>
                                {value.name}
                              </MenuItem>
                            ))}
                          </TextField>
                          <HelpTooltip label={"help text 6"} />
                        </Box>
                      </>
                    )}
                  </Stack>
                </Box>
              </Paper>
              <Stack spacing={2} sx={{ pt: 2 }} direction="row">
                <LoadingButton loading={formik.isSubmitting} color="primary" variant="contained" type="submit">
                  {dataset ? "update" : "Submit"}
                </LoadingButton>
                <Button color="primary" onClick={() => Router.back()}>
                  Cancel
                </Button>
              </Stack>
            </Stack>
          </Form>
        )}
      </Formik>
      <Input
        style={{ display: "none" }}
        type="file"
        name="file"
        id="file"
        variant="outlined"
        onChange={(e) => {
          onSubmitFile(e)
        }}
      />
    </Stack>
  )
}

export default FormDataset
