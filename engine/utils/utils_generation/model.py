from agilkia import Trace,Event
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier,RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
from collections import Counter

from .utils import t_by_clusters
def generate_all_traces(model_name,model, prefix,length=5, action_prob=0.01, path_prob=1.0e-12,
                        partial=True, event_factory=None,verbose=False):

    if model_name.lower() in ['decision trees','multilayerperceptron']:
        results = []
        if event_factory is None:
            event_factory = (lambda action: Event(action,{},{}))
        def depth_first_search(prefix, prob):
            indent = ">" * len(prefix)
            # print(indent + ",".join([ev.action for ev in prefix]))
            if len(prefix) >= length:
                if partial:
                    results.append(Trace(events=prefix, meta_data={"freq":prob}))
            else:
                [proba] = model.predict_proba(prefix)
                for i, p in enumerate(proba):
                    if p >= action_prob and prob * p >= path_prob:
                        action = model.classes_[i]
                        if action == '<END>':
                            results.append(Trace(events=prefix, meta_data={"freq":prob * p}))
                        else:
                            if verbose:
                                print(indent + f" trying {action}")
                            depth_first_search(prefix + [event_factory(action)], prob * p)
        depth_first_search([event_factory(prefix)], 1.0)
        return results
    else:
        raise ValueError('other ML model are not implemented yet')
def train(model_name,ex,traceset_execution,y):
    normalizer=MinMaxScaler()
    list_models_available=['decision trees','multilayerperceptron']
    if model_name.lower() in list_models_available:
        if model_name.lower()=='decision trees':
            model=DecisionTreeClassifier()
            model = Pipeline([
                ("Extractor", ex),
                ("Normalize", normalizer),
                ("Tree", model)  # fast, 0.951
            ])
            model.fit(traceset_execution, y)
        if model_name.lower()=='multilayerperceptron':
            model = MLPClassifier(random_state=1, max_iter=100)

            model = Pipeline([
                ("Extractor", ex),
                ("Normalize", normalizer),
                ("Tree", model)  # fast, 0.951
            ])
            model.fit(traceset_execution, y)
    else:
        raise ValueError('other ML model like '+model_name.lower()+' are not implemented yet, only :'+' ,'.join(list_models_available))
    return model

def generate_tests_per_clusters(model_name,model,execution_clusters,test_clusters,total_number_of_tests):
    #old version with global number of tests --> compute a distribution of tests according to the ratio of traces per cluster
    # counts = dict(Counter(execution_clusters+test_clusters))
    # initial_traces_distrib={}
    #
    # for k,v in counts.items():
    #     initial_traces_distrib[str(k)]=v
    # tests_distrib=t_by_clusters(total_number_of_tests,initial_traces_distrib)

    tests_distrib={}
    for i in range(max(execution_clusters+test_clusters)+1):
        tests_distrib[str(i)]=total_number_of_tests
    tests_per_clusters = {}
    for id_cluster in list(tests_distrib.keys()):
        seqs = generate_all_traces(model_name,model, prefix='<sos_' + str(id_cluster) + '>', length=20, action_prob=0.001,
                                   path_prob=0.001,
                                   partial=True)

        list_seqs=[]
        for seq in seqs:
            trace_list=[]
            for ev in seq.events:
                trace_list.append(ev.action)
            list_seqs.append(trace_list[1:])

        tests_per_clusters[str(id_cluster)] = list_seqs[:tests_distrib[str(id_cluster)]]
    return tests_per_clusters