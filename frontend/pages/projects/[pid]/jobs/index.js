import React from "react"
import { useQuery } from "react-query"
import Link from "next/link"

import Stack from "@mui/material/Stack"
import Avatar from "@mui/material/Avatar"
import FolderIcon from "@mui/icons-material/Folder"
import Typography from "@mui/material/Typography"
import Tabs from "@mui/material/Tabs"
import Tab from "@mui/material/Tab"
import Divider from "@mui/material/Divider"
import Box from "@mui/material/Box"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"

import { getJobs } from "api/api.jobs"
import { getProject } from "api/api.projects"

import Appbar from "components/layouts/appbar"
import Jobs from "components/jobs/items"
import { stringToHslColor } from "helpers"

function LinkTab(props) {
  return (
    <Link href={props.href}>
      <Tab component="a" {...props} sx={{ textTransform: "none" }} />
    </Link>
  )
}

function Index({ params, query }) {
  const { pid } = params

  const {
    isFetching: isFetchingProject,
    isError: isErrorProject,
    data: PROJECT,
    error,
  } = useQuery("project", () => getProject(pid), { retry: false })

  const page = query.page
  const fetchObjects = (page = 1) => getJobs(page, 10, pid, ["name", "description", "status", "project"])

  const {
    isFetching: isFetchingJobs,
    isLoading: isLoadingJobs,
    isError: isErrorJobs,
    data: JOBS,
    refetch,
  } = useQuery(["jobs", page], () => fetchObjects(page), { keepPreviousData: true, refetchInterval: 5000 })

  const [value, setValue] = React.useState(0)
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <Appbar>
      <Stack mt={2}>
        <Stack direction="row" spacing={2}>
          <Avatar sx={{ bgcolor: stringToHslColor(PROJECT?.data[0].name) }}>
            <FolderIcon />
          </Avatar>
          <Typography variant="h4" color="primary">
            {PROJECT?.data[0].name}
          </Typography>
        </Stack>
        <Stack>
          <Box sx={{ display: "flex", justifyContent: "space-between", paddingTop: 2 }}>
            <Tabs value={value} onChange={handleChange} aria-label="nav tabs example">
              <LinkTab key={0} label="Jobs" href={`/projects/${pid}/jobs`} />
              <LinkTab key={1} label="Traces" href={`/projects/${pid}/traces`} />
              <LinkTab key={2} label="Members" href={`/projects/${pid}/members`} />
            </Tabs>
            <Link href={`/projects/${pid}/jobs/new/`} style>
              <Button variant="text" startIcon={<AddCircleOutlineIcon />}>
                Add
              </Button>
            </Link>
          </Box>
          <Divider />
          <main>
            <Jobs
              JOBS={JOBS}
              isLoading={isLoadingJobs || isFetchingProject}
              refetch={refetch}
              pid={pid}
              isError={isErrorJobs || isErrorProject}
            />
          </main>
        </Stack>
      </Stack>
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params, query: context.query } }
}

export default Index

Index.auth = { role: "admin" }
