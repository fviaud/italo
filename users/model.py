from flask_mongoengine import MongoEngine

db = MongoEngine()

class Users(db.Document):
    email = db.EmailField(max_length=200, required=True,unique=True)
    name = db.StringField(max_length=200)
    meta = {'allow_inheritance': True}
    
class UsersLocal(Users):
    passsword_hash = db.DynamicField()
    