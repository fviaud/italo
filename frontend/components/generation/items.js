import React, { useRef } from "react"
import Router from "next/router"
import { styled } from "@mui/material/styles"
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp"
import MuiAccordion from "@mui/material/Accordion"
import MuiAccordionSummary from "@mui/material/AccordionSummary"
import MuiAccordionDetails from "@mui/material/AccordionDetails"
import Typography from "@mui/material/Typography"
import Chip from "@mui/material/Chip"
import Box from "@mui/material/Box"
import Stack from "@mui/material/Stack"
import IconButton from "@mui/material/IconButton"
import Button from "@mui/material/Button"
import Avatar from "@mui/material/Avatar"
import ArrowBackIcon from "@mui/icons-material/ArrowBack"
import DownloadIcon from "@mui/icons-material/Download"
import ArrowForwardIcon from "@mui/icons-material/ArrowForward"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"
import { findUntested } from "helpers"

const stringToColor = (str, saturation = 50, ligthness = 70) => {
  var hash = 0
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash)
  }
  var h = (hash + str.length) % 360
  return "hsl(" + h + ", " + saturation + "%, " + ligthness + "%)"
}

const removeDuplicateEvent = (trace) => {
  let lastEvent = { action: "" };
  const tmpTrace = trace.map(event => ({
    counter: 1,
    action: event
  }));
  const finalTrace = tmpTrace.filter((event) => {
    if (event.action !== lastEvent.action) {
      lastEvent = event
      return true
    } else {
      lastEvent.counter += 1
      return false
    }
  })
  return finalTrace;
}

const Accordion = styled((props) => <MuiAccordion disableGutters elevation={0} square {...props} />)(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}))

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />} {...props} />
))(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "rgba(255, 255, 255, .05)" : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    marginLeft: theme.spacing(1),
  },
}))

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: "1px solid rgba(0, 0, 0, .125)",
}))

export default function CustomizedAccordions({ job, model, isLoading, isError }) {
  const [expanded, setExpanded] = React.useState("panel1")

  const downloadLink = useRef(null)

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false)
  }

  if (isLoading) return <Spinner />
  if (isError) return <Error />

  const data = job.generativeModels.find((item) => item.name === model).results.tests_per_clusters

  const handleDownload = (event) => {
    event.stopPropagation()
    const blob = new Blob([JSON.stringify(data)])
    const fileDownloadUrl = URL.createObjectURL(blob)
    downloadLink.current.href = fileDownloadUrl
    downloadLink.current.download = "export.json"
    downloadLink.current.click()
    URL.revokeObjectURL(fileDownloadUrl)
  }

  const clusteringModel = job["models"].reduce(
    (best, current) => {
      if (best.results?.score < current.results?.score) return current;
      return best
    }, { results: { score: 0 } });
  const untested = findUntested(clusteringModel);

  return (
    <Box mt={2}>
      <Stack direction="row" spacing={2} sx={{ alignItems: "center", paddingTop: 1, paddingBottom: 2 }}>
        <IconButton edge="end" onClick={() => Router.back()}>
          <ArrowBackIcon />
        </IconButton>
        <Typography variant="h6" color="primary">
          {model}
        </Typography>
        <Box sx={{ flexGrow: 1 }} />
        <a style={{ display: "none" }} ref={downloadLink} href="#download-dialog">
          Download json
        </a>
        <Button variant="outlined" startIcon={<DownloadIcon />} onClick={handleDownload}>
          Download
        </Button>
      </Stack>
      {Object.keys(data).map((cluster, indexCluster) => (
        <Accordion expanded={expanded === indexCluster} onChange={handleChange(indexCluster)} key={`cluster-${indexCluster}`}>
          <AccordionSummary
            aria-controls="panel1d-content"
            id="panel1d-header"
            sx={{
              display: "flex",
              backgroundColor: Array.isArray(untested) && untested.indexOf(indexCluster) >= 0 ? "rgba(8, 255, 0, 0.06)" : "rgba(0, 0, 0, .03)"
            }}
          >
            <Typography>Cluster: {cluster}</Typography>
            <Box sx={{ flexGrow: 1 }} />
            {Array.isArray(untested) && untested.indexOf(indexCluster) >= 0 && <Typography>Untested yet</Typography>}
          </AccordionSummary>
          <AccordionDetails>
            {data[cluster].map((trace, indexTrace) => (
              <React.Fragment key={`cluster-${indexCluster}-trace-${indexTrace}`}>
                <Typography>test :{indexTrace}</Typography>
                {/* {trace.map((event, indexEvent) => (
                  <Chip
                    key={`cluster-${indexCluster}-trace-${indexTrace}-event-${indexEvent}`}
                    sx={{ m: 0.5, bgcolor: stringToColor(event) }}
                    label={event}
                    size="small"
                    variant="outlined"
                  />
                ))} */}
                <Box sx={{ display: "flex", alignItems: "center", flexWrap: "wrap" }}>
                  {removeDuplicateEvent(trace).map((event, indexBis) => (
                    <React.Fragment key={`cluster-${indexCluster}-trace-${indexTrace}-event-${indexBis}`}>
                      {indexBis !== 0 && <ArrowForwardIcon />}
                      <Chip
                        sx={{ m: 0.5, bgcolor: stringToColor(event.action) }}
                        label={event.action}
                        size="small"
                        variant="outlined"
                        avatar={event.counter > 1 ? <Avatar style={{ color: "black" }}>{Number.parseInt(event.counter)}</Avatar> : <></>}
                      />
                    </React.Fragment>
                  ))}
                </Box>
              </React.Fragment>
            ))}
          </AccordionDetails>
        </Accordion>
      ))}
    </Box>
  )
}
