import React from "react"

import Model from "components/models/form"
import Appbar from "components/layouts/appbar"
import { useQuery } from "react-query"

import { getModel } from "api/api.models"

export default function Index({ params }) {
  const { mid } = params
  const { data: MODEL, isFetching, isError } = useQuery("MODEL", () => getModel(mid), { retry: false })
  return (
    <Appbar>
      <Model model={MODEL?.data[0]} isFetching={isFetching} isError={isError} />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params } }
}

Index.auth = { role: "admin" }
