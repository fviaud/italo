import * as React from "react"
import Router, { useRouter } from "next/router"
import Link from "next/link"
import Image from "next/image"

import Box from "@mui/material/Box"
import List from "@mui/material/List"
import ListItem from "@mui/material/ListItem"
import ListItemAvatar from "@mui/material/ListItemAvatar"
import ListItemText from "@mui/material/ListItemText"
import Avatar from "@mui/material/Avatar"
import ListItemButton from "@mui/material/ListItemButton"
import Stack from "@mui/material/Stack"
import Pagination from "@mui/material/Pagination"
import FileCopyIcon from "@mui/icons-material/FileCopy"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import Chip from "@mui/material/Chip"
import Paper from "@mui/material/Paper"
import CircularProgress from "@mui/material/CircularProgress"

import Empty from "components/utils/empty"
import Spinner from "components/utils/spinner"
import Error from "components/utils/error"

import { stringToHslColor } from "helpers"

import IconMenus from "./iconMenu"
import philaeImage from "public/philae.png"

export default function Items({ JOBS, isLoading, page = 0, refetch, isError, pid }) {
  const router = useRouter()
  if (isLoading) return <Spinner />
  if (isError) return <Error />

  return (
    <Stack mt={2}>
      {JOBS.data.results.length === 0 && page === 0 && <Empty />}
      <List dense>
        {JOBS.data.results.map((job, index) => (
          <ListItemButton dense disableRipple key={index} onClick={() => Router.push(`/projects/${pid}/jobs/${job._id.$oid}`)}>
            <ListItem
              secondaryAction={
                <Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                  {job.status === "in progress" && (
                    <Paper sx={{ p: 1, display: "flex", alignItems: "center", marginRight: 1 }} elevation={1}>
                      <Typography sx={{ marginRight: 1 }}>Process by</Typography>
                      <Image src={philaeImage} width={70} height={30} alt="Philae" />
                      <CircularProgress size={30} sx={{ marginLeft: 1 }} />
                    </Paper>
                  )}
                  {!(job.status === "in progress") && (
                    <>
                      {job.status && (
                        <Chip
                          size="small"
                          label={job.status[0].toUpperCase() + job.status.substring(1)}
                          color={job.status === "completed" ? "success" : "error"}
                          sx={{ marginRight: 1 }}
                        />
                      )}
                      <IconMenus job={job} refetch={refetch} />
                    </>
                  )}
                </Box>
              }>
              <ListItemAvatar>
                <Avatar sx={{ bgcolor: stringToHslColor(job.name) }}>
                  <FileCopyIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={job.name} secondary={job.description} />
            </ListItem>
          </ListItemButton>
        ))}
      </List>
      {(JOBS.data.totalPages > 1 || JOBS.data.totalPages < page) && (
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            p: 1,
            m: 1,
          }}>
          <Pagination
            count={JOBS.data.totalPages}
            page={parseInt(router.query.page) || 1}
            onChange={(event, value) => Router.push(`/jobs/?page=${value}`)}
          />
        </Box>
      )}
    </Stack>
  )
}
