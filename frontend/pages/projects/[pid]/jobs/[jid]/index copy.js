import React from "react"
import { useQuery } from "react-query"
import Router from "next/router"

import Job from "components/jobs/item"
import Appbar from "components/layouts/appbar"
import { getProject } from "api/api.projects"
import { getJob } from "api/api.jobs"

export default function Index({ params, query }) {
  const { pid, jid } = params
  // const { tab } = query
  const {
    isFetching: isFetchingProject,
    isError: isErrorProject,
    data: PROJECT,
    error: errorProjects,
  } = useQuery("project", () => getProject(pid), { retry: false })

  const { isFetching: isFetchingJob, isError, data: JOB, error } = useQuery("job", () => getJob(jid), { retry: false })

  if (error?.response.status === 401) Router.push("/login")

  return (
    <Appbar>
      <Job
        job={JOB?.data[0]}
        isLoading={isFetchingProject || isFetchingJob}
        isError={isError}
        project={PROJECT?.data[0]}
        // tab={tab}
      />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params, query: context.query } }
}

Index.auth = { role: "admin" }
