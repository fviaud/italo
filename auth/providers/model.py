from flask_mongoengine import MongoEngine
from flask_bcrypt import Bcrypt
import flask_login

db = MongoEngine()
bcrypt = Bcrypt()

class Users(db.Document,flask_login.UserMixin):
    email = db.EmailField(max_length=200, required=True,unique=True)
    name = db.StringField(max_length=200)
    meta = {'allow_inheritance': True}
    def is_active(self):   
        return True           
    def get_id(self):         
        return str(self.id)
    def to_json(self):
        return {
            "email": self.email,
            "id": self.id,
        }

class UsersLocal(Users):
    passsword_hash = db.DynamicField()
    def set_password(self, password):
        self.passsword_hash = bcrypt.generate_password_hash(password).decode('utf-8')
    
    def check_password(self, password):
        return bcrypt.check_password_hash(self.passsword_hash, password)