import React from "react"

import Appbar from "components/layouts/appbarForm"
import Form from "components/traces/formTraces"

export default function Index({ params }) {
  const { pid } = params

  return (
    <Appbar>
      <Form pid={pid} />
    </Appbar>
  )
}

export function getServerSideProps(context) {
  return { props: { params: context.params } }
}

Index.auth = { role: "admin" }
