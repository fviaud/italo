import gridfs
import json
from bson import ObjectId

def get_clustering_dataset(mode,item,db,collection_traces,collection_file,collection_datasets,collection_stores):
    
    if mode =='LOCAL_DEBUG' :
        local_dataset = 'SCANETTE_BIG'

        if local_dataset=='SCANETTE':
            with open('./local_data/1026-steps.json') as json_file:
                data_json1 = json.load(json_file)
            with open('./local_data/functional-tests.json') as json_file:
                data_json2 = json.load(json_file)
        elif local_dataset=='SCANETTE_BIG':
            with open('./local_data/100043-steps.json') as json_file:
                data_json1 = json.load(json_file)
            with open('./local_data/functional-tests.json') as json_file:
                data_json2 = json.load(json_file)
        elif local_dataset=='SPREE':
            with open('./local_data/spree_300_traces_not_grouped.json') as json_file:
                data_json1 = json.load(json_file)
            data_json2=dict()
        elif local_dataset=='TEAMING':
            with open('./local_data/teaming_v4_agilkia_execution.json') as json_file:
                data_json1 = json.load(json_file)
            with open('./local_data/teaming_v4_agilkia_test.json') as json_file:
                data_json2 = json.load(json_file)
        else:
            raise ValueError("local_dataset should be SCANETTE, SPREE or TEAMING")

    elif mode =='SERVER':
            fs = gridfs.GridFS(db)
            data_json1={}
            if type(item["execution_traces"]) is list:
                for id in item["execution_traces"]:
                    store = collection_stores.find_one({"_id": ObjectId(id)})
                    data = collection_file.find_one({"_id": store["file"]})
                    file = fs.get(data['_id']) 
                    if not data_json1 :
                        data_json1 = dict(json.loads(file.read()))
                    else :
                        data_json1["traces"].extend(dict(json.loads(file.read()))["traces"])

            data_json2={}
            if type(item["test_traces"]) is list:
                for id in item["test_traces"]:
                    store = collection_stores.find_one({"_id": ObjectId(id)})
                    data = collection_file.find_one({"_id": store["file"]})
                    file = fs.get(data['_id'])
                    if not data_json2 :
                        data_json2 = dict(json.loads(file.read()))
                    else :
                        data_json2["traces"].extend(dict(json.loads(file.read()))["traces"])
            
    else:
        raise ValueError("mode should be SERVER or LOCAL")

    return data_json1,data_json2
