import axios from "axios"

const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_PROJECTS}`,
  withCredentials: true,
})

export const getJobs = (page, limit = 0, pid, fields = []) => {
  return api.get(`projects/${pid}/jobs`, { params: { page, limit, fields } });
}

export const getJob = (id) => {
  return api.get(`projects/jobs/${id}`)
}

export const delJobs = (id) => {
  return api.delete(`projects/jobs/${id}`)
}

export const addJobs = (object) => {
  return api.post("projects/jobs", object)
}

export const updateJobs = (object) => {
  return api.patch("projects/jobs", object)
}

export const updateStatusJobs = (id, object) => {
  return api.patch(`projects/jobs/${id}/status`, object)
}
