import axios from "axios"

const apiAuth = axios.create({
    baseURL: '/api/auth',
    withCredentials: true,
})

export const logout = () => {
    return apiAuth.get(`signout`);
}
export const getSession = () => {
    return apiAuth.get(`session`);
}