import React from "react"
import { Input, Box } from "@mui/material"
import Button from "@mui/material/Button"
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline"
import { useQueryClient } from "react-query"
import { notifMessage } from "components/utils/notif"
import { addTraces } from "api/api.traces"
import HelpTooltip from "components/helpTooltip"
import UploadIcon from "@mui/icons-material/Upload"

function Form({ refetch, setUpLoading, upLoading }) {
  const queryClient = useQueryClient()
  const curentSession = queryClient.getQueryData("session")

  const onSubmit = async (e) => {
    const formData = new FormData()
    formData.append("trace", file.files[0])
    formData.append("author", curentSession.data._user_id)
    formData.append("public", true)
    try {
      await addTraces(formData)
      setUpLoading(false)
      notifMessage("success", "Add trace with success !")
      refetch()
    } catch (error) {
      notifMessage("error", "Failed to add trace !")
      setUpLoading(false)
      refetch()
    }
  }
  return (
    <Box>
      <label htmlFor={`file`}>
        <Input
          style={{ display: "none" }}
          type="file"
          name="file"
          id="file"
          variant="outlined"
          onChange={(e) => {
            setUpLoading(true), onSubmit(e)
          }}
        />
        {!upLoading && (
          <Button color="secondary" variant="text" startIcon={<UploadIcon />} component="span">
            Upload
          </Button>
        )}
      </label>
      {/* <HelpTooltip label={"help text 4"} color="secondary"/> */}
    </Box>
  )
}

export default Form
