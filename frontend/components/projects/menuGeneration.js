import * as React from "react"
import Box from "@mui/material/Box"
import Tab from "@mui/material/Tab"
import TabContext from "@mui/lab/TabContext"
import TabList from "@mui/lab/TabList"
import TabPanel from "@mui/lab/TabPanel"

// import Details from "./details"
// import Synthese from "./synthese"

export default function LabTabs() {
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  const [value, setValue] = React.useState(0)
  const pages = [
    {
      name: "DecisionTrees",
      // component: <Synthese idcluster={idcluster} />
    },
    {
      name: "Transformers",
      // component: <Details CLUSTER={CLUSTER} query={query} page={page} menu={value} />
    },
    {
      name: "LogisticRegression",
      // component: <Details CLUSTER={CLUSTER} query={query} page={page} menu={value} />
    },
  ]

  return (
    <Box sx={{ width: "100%", typography: "body1" }}>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList onChange={handleChange}>
            {pages.map((item, index) => (
              <Tab style={{ textTransform: "none" }} key={index} label={item.name} value={item.name} />
            ))}
          </TabList>
        </Box>
        {pages.map((item, index) => (
          <TabPanel value={item.name} key={index}>
            {/* {item.component} */}
          </TabPanel>
        ))}
      </TabContext>
    </Box>
  )
}
