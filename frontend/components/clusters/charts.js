import React from "react"
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from "chart.js"
import { Bar } from "react-chartjs-2"

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend)

export const options = {
  indexAxis: "y",
  elements: {
    bar: {
      borderWidth: 2,
    },
  },
  responsive: true,
  plugins: {
    legend: {
      position: "right",
    },
    title: {
      display: false,
      text: "Chart.js Horizontal Bar Chart",
    },
  },
}

export default function App({ cluster_global, cluster }) {
  const labels = Object.keys(cluster_global).map((item) => item)
  const data = {
    labels,
    datasets: [
      {
        label: "Global",
        data: labels.map((item) => cluster_global[item]),
        // borderColor: "rgb(255, 99, 132)",
        borderColor: "#FDC287",
        backgroundColor: "#ffcf9f",
      },
      {
        label: "Cluster",
        data: labels.map((item) => cluster[item]),
        // borderColor: "rgb(53, 162, 235)",
        borderColor: "#85DADA",
        backgroundColor: "#a5dfdf",
      },
    ],
  }

  return <Bar options={options} data={data} /*redraw={true}*//>
}
